\version "2.19.82"

%Altérations suggérées par l'éditeur
editaltb = {
  \set suggestAccidentals = ##t
  \override Staff.AccidentalSuggestion.parenthesized = ##f
  \override Staff.AccidentalCautionary.font-size = #-5
}
editalte = {
  \set suggestAccidentals = ##f
}

%Trille "t"
trt = ^\markup \italic "t"

%piano en toutes lettres
pianol = _\markup {\italic "piano"}

%forte en toutes lettres
fortel = \markup {\italic "forte"}

%Segno
alsegno = \mark \markup{\musicglyph #"scripts.segno"}

%Da capo al segno en toutes lettres
dcas = \mark \markup {\fontsize #-1 \italic "Da capo al segno"}

dc = \mark \markup {\fontsize #-1 \italic "Da capo"}

fine = \mark \markup {\fontsize #-1 \italic "Fine"}

%Tutti
tutti = ^\markup {\italic "Tutti"}

%Senza Cembalo
senzacemb = ^\markup \italic "Senza Cem."

%Entre parenthèses
paren = #(define-event-function (parser location dyn) (ly:event?)
   (make-dynamic-script
    #{ \markup \concat {
         \normal-text \italic \fontsize #2 (
	 \pad-x #0.2 #(ly:music-property dyn 'text)
	 \normal-text \italic \fontsize #2 )
       }
    #}))

%Fermata sur barline
fermatabar = {
  \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup { \musicglyph #"scripts.ufermata" }
}

%Grandes parenthèses
grpar = {\once \override ParenthesesItem.font-size = #0 }

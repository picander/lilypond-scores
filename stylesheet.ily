\version "2.19.82"

\paper {
  #(define fonts
     (set-global-fonts
      #:music "emmentaler"
      #:brace "emmentaler"
      #:roman "Perpetua"
      #:sans "Perpetua"
      #:typewriter "Perpetua"
      #:factor (/ staff-height pt 20)
      ))

  %Marges
  top-margin = 10
  left-margin = 20
  right-margin = 20
  bottom-margin = 15

  %Style for header:piece
  scoreTitleMarkup = \markup {
    \override #'(font-name . "Perpetua")
    \fontsize #4
    \fromproperty #'header:piece
  }

  markup-markup-spacing = #'((basic-distance . 10)
                             (padding . 5))

  top-markup-spacing = #'((basic-distance . 10)
                          (minimum-distance . 10)
                          (padding . 5))
}

#(set-global-staff-size 20)

%Commandes Scheme
%Commande scoretitle
#(define-markup-command (scoretitle layout props text) (markup?)
   #:properties ((fontname "Perpetua Titling MT")
                 (fontsize 5)
                 )
   "Properties for Score Title"
   (interpret-markup layout props
     #{\markup \override #`(font-name . ,fontname)
       \override #`(font-size . ,fontsize)
       { #text } #}))

%Commande scoresubtitle
#(define-markup-command (scoresubtitle layout props text) (markup?)
   #:properties ((fontname "Perpetua")
                 (fontsize 4)
                 )
   "Properties for Score Subtitle"
   (interpret-markup layout props
     #{\markup {\vspace #2 {\override #`(font-name . ,fontname)
                            \override #`(font-size . ,fontsize)
                            { #text }} } #}))

%Commande scoresubsubtitle
#(define-markup-command (scoresubsubtitle layout props text) (markup?)
   #:properties ((fontname "Perpetua")
                 (fontsize 2)
                 )
   "Properties for Score Subsubitle"
   (interpret-markup layout props
     #{\markup \override #`(font-name . ,fontname)
       \override #`(font-size . ,fontsize)
       { #text } #}))

%Commande scorecomposer
#(define-markup-command (scorecomposer layout props text) (markup?)
   #:properties ((fontname "Perpetua")
                 (fontsize 3)
                 )
   "Properties for Score Composer"
   (interpret-markup layout props
     #{\markup \override #`(font-name . ,fontname)
       \override #`(font-size . ,fontsize)
       { #text } #}))

%Commande scoreinstrument
#(define-markup-command (scoreinstrument layout props text) (markup?)
   #:properties ((fontname "Perpetua")
                 (fontsize 4)
                 )
   "Properties for Score Instrument"
   (interpret-markup layout props
     #{\markup {\vspace #2 \override #`(font-name . ,fontname)
                \override #`(font-size . ,fontsize)
                { #text }} #}))

\layout {
  \override FiguredBass.BassFigure #'font-size = #-1
  \override FiguredBass.BassFigure.font-name = "OpusFiguredBassStd"
  \context {
    \Score
    \override MetronomeMark.font-name = "Perpetua"
    \override MetronomeMark.font-shape = #'italic
    \override MetronomeMark.font-size = #3
    \override InstrumentName.font-name = "Perpetua"
  }
  \context {
       \Lyrics 
         \override LyricText #'font-name = "Perpetua" 
    }
}
\version "2.19.82"
\include "../../commands.ily"
\include "../../stylesheet.ily"


\header {
  title = \markup \scoretitle {"Ah, mio caro bene (Rodelinda)"}
  subtitle = \markup \scoresubtitle {"Rodelinda, HWV19"}
  subsubtitle = \markup \scoresubsubtitle {"Atto III, Sc. 8"}
  composer = \markup \scorecomposer {"Georg Friedrich Haendel"}

  tagline = \markup {\override #'(font-name . "Perpetua") "Typesetting by Picander with Lilypond. License CC BY-NC 4.0." }
}
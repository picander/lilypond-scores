\version "2.19.82"

\include "../../commands.ily"

obvl = \relative c'' {
  \clef treble
  \key g \major
  \time 4/4
  \tempo "Allegro"

  r4 r8 g d'4~d8 c16 b | a8 g r4 c8 b r4 | e8 d r g, g'4~g8 fis16 e | d8 g,4 a8 b c d c16 b | b8 a r g' fis-! r8 a,-! r |
  d-! r fis,8. g32 a d,8 c' b a16 g | g4 r8 g'\pp ^\markup {"Viol. (senza Oboe.)"} fis16( d c b) e( c b a) | d( b a g) c( a g fis) b g a b d,8 fis | g4 r8 g b4~b8 a16 g | fis8 g r4 fis'8 g r4 | c,8 b r g b c d g, |
  fis8 g r4 r r8 g | g fis  r4 r2 | R1 | R1 |
  R1 | R1 | cis'8 r d r g, r fis r | e r r cis' d4 r | r r8 d'\f cis-! r e,-! r |

  a8-! r cis,8.\trill d32 e a,8 g' fis e | d a d,4 r2 | R1 | R1 | r4 r8 d'\p
  g8 c, c c | g' b, b b g' a, a a | a4 r r2 | r8 d g2 fis4 | g r8 b, a4 r8 a' |
  d,4 r8 b a4 r8 a' | d,4 r8 g fis16( d c b) e( c b a) | d( b a g) c( a g fis) b( g fis e) a( fis e d) | b'8 a16 g fis8 d' g,4 r |
  R1 | r4 r8 g\f d'4~d8 c16 b | a8 g r4 c8 b r4 | e8 d r d g c, c c | g' b, b b g' a, a a |
  a4 r8 g' fis-! r a, r | d r fis,8.(\trill g32 a) d,8 c' b a | g4 r8 g'\pp fis16( d c b) e( c b a) | d( b a g) c( a g fis) b( g a b) d,8 fis | g4\fermata \bar "|."
}
\version "2.19.82"

\include "../../commands.ily"

basso = \relative c {
  \clef bass
  \key g \major
  \time 4/4
  \tempo "Allegro"

  g4 r8 g' g a b c | d e b c a g d e | c g r g' g, a b c | d e4 fis8 g a b c | d d, r e d-! r c-! r |
  b-! r a-! r b g d' d, | g4 r8 e''\pp d r c r | b r a r g c, d d, | g4 r8 g g' a b c | d e b c a g d e | c g r g' g, a b c |
  d8 e4 fis8 g a b c | d d, r fis g fis e fis | g fis e fis g fis e d | a'4 r8 fis g e a cis, |
  d4 r8 b' g e a cis | d4 r8 fis, g e a e | a, r a r a r a r | a' g fis e d g a a, | d e fis d\f a' r g r |

  fis8 r e r fis b a a, | d4 r8 d' b g b c | d d, r d e c e fis | g g, b d g a b c | d d, r d'
  e4 r | d r c r8 c | d d, d' c b c d d, | g fis e b c a d d, | g' fis e d c a d fis |
  g8 fis e d c a d fis | g a b g d' r c r | b r a r g r fis r | g c d d, e4 r8 fis |
  g4 c, d d, | g8 d'\f b g g' a b c | d e b c a g d e | c g  r g' e4 r | d r c r |
  d4 r8 e' d r c r | b r a r b g d' d, | g a b g\pp d r c r | b r a r g c d d, | g4\fermata \bar "|."
}

chiffres = \figuremode {
r1 | r4 <6> <6> r | r1 | r4. <6>8 r4 <6> | <6 4>8 <5 3>8 r8 r <6> r <6> r |
<6> r <6> r r4 <6 4>8 <5 3> |
 r2 <6>8 r <6>8 r | <6> r <6> r r2 | r1 | r4 <6> <6> r | r1 |
 r4. <6>8 r2 | <6 4>8 <5 3>8 r <6> <6> <6> <6/> <6> | <6> <6> <6/> <6> <6> <6> <6/>4 | <_+>4 r8 <6> <6 5>4 <_+>8 <6> |
 r2 <6 5>4 <_+>8 <6> | r4. <6>8 <6 5>4 <_+> | <7 _+>4 <6 4> <_+> <6 4> | <_+> <6>8 <6/> r4 <4>8 <_+> | r2 <_+>4 <6/> |

 r4 <6/> r <6 4>8 <5 _+> | r2 <6>4 r | r2 <6>2 | r1 | <6 4>8 <5 3> r4
 <6>2 | <6 4>2 <6 5> | r <6> | r <6 5> | r <6 5> |
 r2 <6 5> | r r4 <6> | <6> <6> r <6> | r2 r4 r8 <6> |
 r2 <6 4>4 <5 3>4 | r1 | r2 <6> | r <6> | <6 4> <6 5> |
 r2 <6>4 <6> | <6> <6> <6>2 | r <6>4 <6> | <6> <6> r2 | r4
}
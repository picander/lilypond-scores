\version "2.19.82"

\include "../../commands.ily"

viola = \relative c'' {
  \clef alto
  \key g \major
  \time 4/4
  \tempo "Allegro"

  r4 r8 g b4~b8 a16 g | fis8 g r4 fis8 g r4 | g8 b r8 b b4~b8 a16 g | fis8 g4 d8 g fis16 e d8 g | g fis r8 g b-! r fis-! r |
  g-! r c,-! r b e d fis | g4 r r2 | r2 r8 e\pp d c | b4 r r2 | r4 d8\pp e r4 fis8 g | g,4 r r2 |
  R1 | R1 | R1 | R1 |
  R1 | R1 | e'8 r fis r cis r d r | cis r r e fis4 r | r r8 fis\f e r cis' r |

  d8-! r g, r fis d a' g | fis4 r r2 | R1 | R1 | R1 |
  r2 r4 r8 e | fis4 r r2 | R1 | R1 |
  R1 | R1 | R1 | R1 |
  R1 | r4 r8 b,8\f b'4~b8 a16 g | fis8 g d e fis g r4 | c8 b r b g4 r | b8 g g g e g g g |
  fis4 r8 e a r fis r | g r c, r d e d c | b4 r8 b' b, r c r | d r fis r g e b a | b4\fermata \bar "|."
}
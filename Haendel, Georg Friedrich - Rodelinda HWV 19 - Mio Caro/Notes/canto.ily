\version "2.19.82"

\include "../../commands.ily"

canto_notes = \relative c'' {
  \clef treble
  \key g \major
  \time 4/4
  \autoBeamOff
  \tempo "Allegro"

  R1 | R1 | R1 | R1 | R1 |
  R1 | R1 | R1 | r4 r8 g d'4~d8 c16[ b] | a8 g r4 c8 b r4 | e8 d r g, g'4~g8 fis16[ e] |
  d8 g,4 a8 b c d c16[ b] | b8 a r d e d cis d | e[( d cis)] d e d g fis | e a, d4~d8[ e16 d] cis[ b] a[ g] |
  fis8[ a] d4~d8[ e16 d] cis[ b a g] | fis8[ a] d4~d8[ e16 d] cis[ b cis a] | g'4-! fis-! e-! d-! | a8[ b16 cis d8] g fis e16[ d] a8 cis | d4 r r2 |

  R1 | r4 r8 a d4~d8 c16[ b] | a[ g] a8 r d g4~g8 fis16[ e] | d8 g,4 a8 b c d c16[ b] | b8 a r d
  g8 c, c c | g' b, b b g' a, a a | a4 r8 e' d c16[ b] a8 g | g4~g16[ b a g] a[ g a8~] a16[ c b a | b8] d g4~g8[ a16 g]fis[ e] d[ c] |
  b8[ d] g4~g8[ a16 g] fis[ e d c] | b[ a] g8 r g' fis4-! a,-! | d-! fis-! g-! a-!| d,8 c16[ b] a8 fis' g4~g8 fis16[ e] |
  d4. e8 b4( a8.) g16 | g4 r r2 | R1 | R1 | R1 |
  R1 | R1 | R1 | R1 | r4\fermata \bar "|."

}

canto_text = \lyricmode {
  Mio ca -- ro be -- ne! ca -- ro, ca -- ro! mio ca -- ro
  be -- ne! non ho più~af -- fan -- nie pe -- ne, non ho più~af -- fan -- ni~al cor, __ non ho più pe -- ne~al cor, non ho __ più af -- fan -- - - - - - - ni, nò, non ho, __ non ho più pe -- ne~al cor;

  mio ca -- ro be -- ne, mio ca -- ro be -- ne! non ho più~af -- fan -- ni~e pe -- ne, non
  ho più~af -- fan -- ni~e pe -- ne, nò, non ho più pe -- ne~al cor, non ho più pe -- ne~al cor, __ _  non ho __ più af --
  fan -- -- - - - ni, non ho più~af -- fan -- ni, nò, non ho più pe -- ne~al cor, __ non |
  ho più pe -- ne~al cor.
}
\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../Notes/tutti.ily"
\include "../Notes/viola.ily"
\include "../Notes/canto.ily"
\include "../Notes/basso.ily"

#(set-global-staff-size 18)

\score {
  <<
    \new StaffGroup {
      <<
        \new Staff \with {
          instrumentName = "Tutti."
          shortInstrumentName = "T."
          midiInstrument = "violin"
        }\obvl

        \new Staff \with {
          instrumentName = "Violino III, e Viola."
          shortInstrumentName = "Vl.III e Vla."
          midiInstrument = "viola"
        }\viola

      >>
    }
    \new Staff \with {
      vocalName = "Rodelinda."
      shortVocalName = "Rod."
    }
    <<
      \new Voice = "canto" {\canto_notes}
    >>
    \new Lyrics \lyricsto "canto" {\canto_text}

    \new StaffGroup {
      \override StaffGroup.SystemStartBracket.collapse-height = #4
      \override Score.SystemStartBar.collapse-height = #4
    <<
      \new Staff \with{
        instrumentName = "Basso"
        shortInstrumentName = "Basso"
        midiInstrument = "cello"
      }{\basso} \new FiguredBass \chiffres
    >>
    }
  >>
  \layout {}
  \midi {}
}
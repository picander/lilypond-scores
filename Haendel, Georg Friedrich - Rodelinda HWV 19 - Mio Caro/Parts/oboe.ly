\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "stylesheet-parts.ily"

\include "../Notes/tutti.ily"

#(set-global-staff-size 20)

\header {
  instrument = \markup \scoreinstrument "Oboe/Violini I-II"
}

\score {
  \obvl
}
\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "stylesheet-parts.ily"

\include "../Notes/viola.ily"

#(set-global-staff-size 20)

\header {
  instrument = \markup \scoreinstrument "Violino III/Viola"
}

\score {
  \viola
}
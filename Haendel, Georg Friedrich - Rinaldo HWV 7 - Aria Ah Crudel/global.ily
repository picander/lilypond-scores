\version "2.19.82"
\include "../../stylesheet.ily"

\header {
 title = \markup \scoretitle {"Ah Crudel! (Armida)"}
 subtitle = \markup \scoresubtitle {"Rinaldo, HWV 7"}
 subsubtitle = \markup \scoresubsubtitle {"Atto II, Sc. 8"}
 composer = \markup \scorecomposer {"Georg Friedrich Haendel"}

 tagline = \markup {\override #'(font-name . "Perpetua") "Typesetting by Picander with Lilypond. License CC BY-NC 4.0."}
}
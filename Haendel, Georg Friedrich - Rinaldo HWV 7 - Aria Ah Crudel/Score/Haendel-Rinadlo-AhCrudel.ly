\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "../Notes/oboe.ily"
\include "../Notes/basson.ily"
\include "../Notes/violinoI.ily"
\include "../Notes/violinoII.ily"
\include "../Notes/violinoIII.ily"
\include "../Notes/violetta.ily"
\include "../Notes/violoncello.ily"
\include "../Notes/armida.ily"
\include "../Notes/contrabasso.ily"

#(set-global-staff-size 14)

\score {
  <<
    \new StaffGroup {
      <<
        \new Staff \with{
          instrumentName = "Oboe"
          shortInstrumentName = "Ob."
          midiInstrument = "oboe"
        }\oboe
        \new Staff \with{
          instrumentName = "Basson"
          shortInstrumentName = "Bsn."
          midiInstrument = "bassoon"
        }\basson
      >>
    }
    \new StaffGroup {
      <<
        \new Staff \with{
          instrumentName = "Violino I"
          shortInstrumentName = "Vl. I"
          midiInstrument = "violin"
        }\violinoI
        \new Staff \with{
          instrumentName = "Violino II"
          shortInstrumentName = "Vl. II"
        }\violinoII
        \new Staff \with{
          instrumentName = "Violino III"
          shortInstrumentName = "Vl. III"
          midiInstrument = "violin"
        }\violinoIII
        \new Staff \with{
          instrumentName = "Violetta"
          shortInstrumentName = "Vla."
          midiInstrument = "viola"
        }\violetta
        \new Staff \with{
          instrumentName = "Violoncello"
          shortInstrumentName = "Vcl."
          midiInstrument = "cello"
        }\violoncello
      >>
    }
    \new StaffGroup {
      <<
        \new Staff \with{
          vocalName = "Armida"
          shortVocalName = "Arm."
          midiInstrument = "voice oohs"
        }
        <<
          \new Voice = "armida" {\armidamusic}
        >>
          \new Lyrics \lyricsto "armida" {\armidalyrics}
      >>
    }

    \new StaffGroup {
      \override StaffGroup.SystemStartBracket.collapse-height = #4
      \override Score.SystemStartBar.collapse-height = #4
      \new Staff \with{
        instrumentName = "Contrabasso"
        shortInstrumentName = "Cb."
        midiInstrument = "contrabass"
      }\contrabasso
    }
  >>
  \layout {
  }
  \midi {
  }
}

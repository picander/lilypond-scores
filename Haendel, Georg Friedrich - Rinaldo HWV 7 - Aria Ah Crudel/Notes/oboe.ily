\version "2.19.82"

oboe = \relative c'' {
  \clef treble
  \key g \minor
  \time 4/4
  \tempo "Largo"

  R1 | r2 g'~| g4. bes,16 a a4. c16 bes | bes8 es16 d d8 g, fis d c'4 |r8 bes g'4 r8 f16. es32 f4 | r8 bes, es4 r8 d16. c32 d4 |
  r8 g16 c, c4 r8 a'16\p bes, bes4 | r8 aes16. g32 aes4\pp r8 g16 fis g4 | r8 g fis4 r8 g d'4 | r8 c16. bes32 c4 r8 c a'4 |\alsegno r8 g, c4 r8 d g,4 | r8 d c'4 r8 a fis'4 | r2 r8 d, d'4 |

  R1 | r8 es c'4 r8 f, bes4 | r8 d, bes'4 r8 c, a'4 | r8 c, a' fis d4 r | r2 r8 es16. d32 d4 | r8 es16. d32 d4 r8 d g,4 |
  r8 c16. bes32 bes4 r8 a16. g32 g4 | r8 f16. es32 es4 r2 | r r8 es'16.\f d32 d4 | r8 es16. d32 d4 r8 g16. f32 f4 | r8 bes16 a a4 r8 c,16\p bes bes4 | r8 aes16\pp g fis8 d d'8. es16 bes8 a16 g | g4\fermata \fine r r2 \bar "|."

\tempo "Presto" d'16 d f f d d f f bes,4 r | g'16 g bes bes g g bes bes es,4 r | d16 d f f d d f f bes, bes f' f bes, bes f' f | g g es es c c a' a bes4 r |
  r2 a,16 a c c a a c c | f,4 r g16 g bes bes g g bes bes | es,4 r r2 | R1 |

  R1 | a'16 a c c a a c c f, f a a f f a a | d,4 r e16 e a a e e a a | f4 r r2 |
  R1 | r2 d16 d c c bes bes a a | bes bes a a g g f f g8 e a a, | d4\mark \markup "(Largo)" r r2 | r a'4 r8 bes \bar "||"\dcas

}
\version "2.19.82"

violinoIII = \relative c' {
  \clef treble
  \key g \minor
  \time 4/4
  \tempo "Largo"

  R1 | R1 | R1 | r2 r4 r8 fis | g d' bes4 r8 a d4 | r8 bes g4 r8 f bes4 |
  r8 bes a4 r8 a g4 | r8 es es r r es a,4 | r8 bes16. a32 a4 r8 g' bes4 | r8 a16. g32 fis4 r8 a c4 |\alsegno r8 bes a4 r8 d c,4 | r8 a' fis4 r8 a c4 | r2 r8 fis, a4 |

  r2 r8 g c, es | f4 r r8 f' d4 | d2 a~|a g4 r | r2 r8 bes16. a32 a4 | r8 bes16. a32 a4 r8 a16 bes bes4 |
  r8 c es,4 r8 a c,4 | r8 f c4 r2 | r2 r8 bes'16.\paren \f a32 a4 | r8 bes16. a32 a8 fis g4 r8 c, | g'4 r8 d es4 r8 bes\paren \p | c4 r8 fis\paren \pp d d d d | d4\fermata \fine r r2 \bar"|."

  \tempo "Presto" bes'16 bes c c bes bes c c g4 r | bes16 bes d d bes bes d d g,4 r | bes16 bes c c bes bes c c bes bes a a g g bes bes | bes bes bes bes a a es' es d4 r |
  r2 f,16 f a a f f a a | d4 r es,16 es f f es es d d | c4 r r2 | R1 |

  R1 | f'16 f  g g f f e e d d e e d d cis cis | d4 r cis16 cis e e cis cis e e | d4 r r2 |
  R1 | r2 d16 d c c bes bes a a | bes bes a a g g f f g8 e a a, | d4\mark \markup "(Largo)" r r2 | r r8 a' c4 \bar "||"\dcas

}
\version "2.19.82"

violoncello = \relative c {
  \clef bass
  \key g \minor
  \time 4/4
  \tempo "Largo"

  R1 | R1 | R1 | r2 r4 r8 d | \clef tenor g d' bes4 r8 d f,4 | r8 es g4 r8 bes \clef bass bes,4 |
r8 es' es,4 r8 d g4 r8 c c, r r es cis4 | r8 d d'4 r8 bes g4 | r8 a a,4 r8 d' d,4 |\alsegno r8 g a4 r8 bes c4 | r8 d, d'4 r8 d d,4 | r2 r8 d' c4 |

R1 | r4 r8 a bes4 r8 a | g4 r8 g a4 r8 g | fis4 r8 d g4 r | r2 r8 \clef tenor g'16. fis32 fis4 | r8 g16. fis32 fis4 r8 d bes4 |
\clef bass r8 a16. g32 g4 r8 f es4 | r8 d c4 r8 bes a4 | r2 r4 r8 g'\paren \f | fis4 r8 d bes'4 r8 a16 g | g4 r8 f es4 r8 d\paren \p | c4 r8 d16\paren \pp c bes8 g d' d, | g4\fermata \fine r r2 \bar "|."

\tempo "Presto" bes'16 bes a a bes bes f f g g f f g g d d | es es d d es es bes bes c c c' c a a f f | bes bes a a bes bes f f g g f f g g d d | es es c c f f f, f bes4 r |
r2 f'16 f e e f f c c | d4 r es16 es d d es es bes bes | c4 r r2 | R1 |

R1 | f16 f e e f f c c d d cis cis d d a a | bes4 r a'16 a cis cis a a cis cis | d4 r r2 |
R1 | r2 d16 d c c bes bes a a | bes bes a a g g f f g8 e a a, | d4\mark \markup "(Largo)" r r2 | r2 r8 d' d,4 \bar "||"\dcas
}
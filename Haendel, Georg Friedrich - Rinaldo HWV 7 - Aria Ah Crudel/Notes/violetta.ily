\version "2.19.82"

violetta = \relative c' {
  \clef alto
  \key g \minor
  \time 4/4
  \tempo "Largo"

  R1 | R1 | R1 | r2 r4 r8 a | bes bes' g4 r8 d a'4 | r8 g bes4 r8 bes d,4 |
  r8 g g4 r8 fis d4 | r8 c c r r bes e4 | r8 d fis,4 r8 bes g'4 | r8 c, a'4 r8 fis a4 |\alsegno r8 g f4 r8 f es4 | r8 d a'4 r8 c, a'4 | r2 r8 a fis4 |

  r2 r8 g c, es | f4 r r8 d bes4 | bes'2 a~|a bes4 r | r2 r8 g16. fis32 fis4 | r8 bes16. a32 a4 r8 fis d bes'16 a |
  a4 r8 g16. f32 f4 r8 es16. d32 | d4. c16. bes32 bes4 r | r2 r8 g'16.\paren \f fis32 fis4 | r8 g16. fis32 fis4 r8 bes16. a32 a4 | r8 g16 f f4 r8 es16\paren \p d d4 | r8 c16\paren \pp c c8 a bes d d d | bes4\fermata \fine r r2 \bar "|."

  \tempo "Presto" f'16 f a a f f a a d,4 r | es16 es f f es es f f c'4 r | f,16 f a a f f a a d, d d d d d d d | bes bes c c c c f f f4 r |
  r2 c16 c c c c c a' a | f4 r bes,16 bes bes bes bes bes bes bes | g4 r r2 | R1 |

  R1 | c'16 c c c c c c c a a a a a a a a | f4 r a16 a a a a a a a | a4 r r2 |
  R1 | r2 d,16 d c c bes bes a a | bes bes a a g g f f g8 e' a a, | d4\mark \markup "(Largo)" r r2 | r2 r8 fis a4 \bar "||"\dcas
}
\version "2.19.82"

contrabasso = \relative c' {
  \clef bass
  \key g \minor
  \time 4/4
  \tempo "Largo"

  r8 g bes a g f es d | c a d d, g' bes es, c | g' g, c g d'4 d'8 fis, | g8 c16 bes bes8 es16 d d4 r8 d,\tutti | g4 r8 g d'4 r8 d, | g4 r8 es bes'4 r8 g, |
  es'4 r8 a, fis'4 r8 g | c,4 r8 d es4 r8 cis | d4 r8 d g4 r8 g | a4 r8 c d,4 r8 bes |\alsegno g'4 r8 a bes4 r8 c | d4 r8 a fis4 r8 d | g8.[ f16 es8. d16] d4 r8 c' |

  b4  r8 g c,4 r8 bes' | a4 r8 a bes4 r8 a | g4 r8 g a4 r8 g | fis4 r8 d g f es g | es' bes16 c d8 d, g,4 r8 g' | fis4 r8 g fis d' bes4 |
  r8 16. g32 g4 r8 f es4 | r8 d c4 r8 bes a g' | fis16 d g c, d8 d, g4 r8 g'\f | fis4 r8 d bes'4 r8 a16 g | g4 r8 f es4 r8 d\p | c4 r8 d16 c bes8 g d' d, | g4\fermata \fine r4 r2 \bar "|."

  \tempo "Presto" bes'16 bes a a bes bes f f g g f f g g d d | es es d d es es bes bes c c c' c a a f f | bes bes a a bes bes f f g g f f g g d d | es es c c f f f, f bes4 r |
  r2 f'16 f e e f f c c | d4 r es16 es d d es es bes bes | c4 r8 c' a f bes g | a f bes g a c f, c |

  d8 f g bes e, f c' c, | f16 f e e f f c c d d cis cis d d a a | bes8 d g bes a16 a cis cis a a cis cis | d,8 d' bes g r c a f |
  r8 bes g e a g f e | d g a a, d'16 d c c bes bes a a | bes bes a a g g f f g8 e a a, | d4\mark \markup "(Largo)" r r2 | r4 r8 a' d,4 r8 bes \bar "||"\dcas
}
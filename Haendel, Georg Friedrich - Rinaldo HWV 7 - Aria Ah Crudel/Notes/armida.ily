\version "2.19.82"

armidamusic = \relative c'' {
  \clef treble
  \key g \minor
  \time 4/4
  \autoBeamOff
  \tempo "Largo"

  R1 | R1 | R1 | R1 | R1 | R1 |
  R1 | R1 | r4 d2~d8 es | fis,2 fis'4. g8 | es4. f!8 d4. g,8 | fis[ g] a4 c4. d8 | bes16[ g] es'[ d] c8. d16 d4 r |

  r8 f4 g8 es4 r | r8 c f, es' es d r4 | r8 bes es, d' d c r4 | r8 c4 d8 bes g g' es | c d bes a16[ g] g4 r8 es' | d4 r8 es8 d4 r8 g |
  f4 r8 es d4 r8 c | bes4 r8 aes16.[ g32] g4 c8. c16 | c8 d16[ es] bes8 a16[ g] g4 r | R1 | R1 | R1 |  R1 \bar "|."

  \tempo "Presto" R1 | R1 | R1 | r2 r4 f'8 d |
  bes es c8. bes16 a8 f r c'16 c | f[ g f es] d[ c bes a] g4 r8 g16 g | es'[ f es d] c[ bes a g f8] c' d e | f[ c16 e] d[ f e g] f[ g e g] f[ g e g] |

  f16[ e d c] bes[ a g f c'8] bes16[ a] a8 g16[ f] | f4 r r r8 f'16 e | d8 f e8. d16 cis8 a r e'16 e | f[ e d e] f[ a g f] e[ d c d] e[ g f e] |
  d16[ cis d e] d[ f e d] cis[ b a8] r d16 e | f8 e16[ d] cis8. d16 d2~| d r8 e a, cis | d4\mark \markup "(Largo)" d2~d8 es | fis,2 fis'4. g8 \bar "||"\dcas
}

armidalyrics = \lyricmode {
  Ah! __ cru -- del, ah! cru -- del, il pian -- to mi -- o deh! ti mo -- va per pie -- tà!
  ah! cru -- del, il pian -- to mi -- o, il pian -- to mi -- o deh! ti mo -- va, deh! ti mo -- va per pie -- tà, pie -- tà, pie -- tà! cru -- del, pie -- tà! cru -- del, pie -- tà! deh! ti mo -- va per pie -- tà!

  O~in -- fi -- del al mio de -- si -- o pro -- ve -- rai, __ _ _ pro -- ve -- rai __ _ la cru -- del -- tà, __ _ _ _ _ _ la cru -- del -- tà,
  o~in -- fe -- del al mio de -- si -- o pro -- ve -- rai, __ _ _ _ _ _ _  pro -- ve -- rai la cru -- del -- tà __ la cru -- del -- tà. Ah! __ cru -- del, ah! cru-
}
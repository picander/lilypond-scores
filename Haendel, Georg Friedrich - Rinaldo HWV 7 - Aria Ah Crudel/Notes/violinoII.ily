\version "2.19.82"

violinoII = \relative c' {
  \clef treble
  \key g \minor
  \time 4/4
  \tempo "Largo"

  R1 | R1 | R1 | r2 r8 d c'4 | r4 r8 g g f r f | es4 r8 es es d r d |
  es4 r8 es d4 r8 d | c4 r8 bes es4 r8 g, | d'4 r8 d d4 r8 g | fis4 r8 fis a4 r8 bes |\alsegno bes4 r8 f f4 r8 c' | fis,4 r8 fis c'4 r8 fis, | g[ d es es'] a,4 r |
  r8 d g, b c4 r | r r8 f, bes4 r8 f | g4 r8 bes es,4 r8 es' | d4 r r2 | r2 r4 r8 g, | d4 r8 g d4 r8 bes'16 a |
  a4 r8 g16. f32 f4 r8 es16. d32 | d4 r8 c16. bes32 bes4 r | r2 r4 r8 bes'16.\paren \f a32 | a4 r8 c16 bes bes4 r8 c16 bes | bes4 r8 a16 g g4 r8 f16\paren \p es | es4 r8 d\paren \pp g g g fis | g4\fermata \fine r4 r2 \bar"|."

  \tempo "Presto" d'16 d f f d d f f bes,4 r | g'16 g bes bes g g bes bes es,4 r | d16 d f f d d f f bes, bes f' f bes, bes f' f | g g es es c c a' a bes4 r |
  r2 a,16 a c c a a c c | f,4 r g16 g bes bes g g bes bes | es,4 r r2 | R1 |

   R1 | a'16 a c c a a c c f, f a a f f a a | d,4 r e16 e a a e e a a | f4 r r2 |
   R1 | r2 d16 d c c bes bes a a | bes bes a a g g f f g8 e a a, | d4\mark \markup "(Largo)" r r2 | r a'4 r8 bes \bar "||"\dcas
}
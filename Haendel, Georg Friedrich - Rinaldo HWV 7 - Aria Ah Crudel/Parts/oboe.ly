\version "2.19.82"

\include "../../stylesheet-parts.ily"
\include "../../commands.ily"

\include "../Notes/oboe.ily"

\include "../global.ily"

#(set-global-staff-size 18)

\header {
  instrument = \markup \scoreinstrument "Oboe"
 }

 \score {
  \oboe

 }
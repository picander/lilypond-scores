\version "2.19.82"

\include "../../stylesheet-parts.ily"
\include "../../commands.ily"

\include "../Notes/violetta.ily"

\include "../global.ily"

#(set-global-staff-size 20)

\header {
  instrument = \markup \scoreinstrument "Violetta"
 }

 \score {
  \violetta

 }
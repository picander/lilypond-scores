\version "2.19.82"

\include "../../stylesheet-parts.ily"
\include "../../commands.ily"

\include "../Notes/basson.ily"

\include "../global.ily"

#(set-global-staff-size 18)

\header {
  instrument = \markup \scoreinstrument "Basson"
 }

 \score {
  \basson

 }
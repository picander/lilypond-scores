\version "2.19.82"

\include "../../stylesheet-parts.ily"
\include "../../commands.ily"

\include "../Notes/violinoI.ily"

\include "../global.ily"

#(set-global-staff-size 18)

\header {
  instrument = \markup \scoreinstrument "Violino I"
 }

 \score {
  \violinoI

 }
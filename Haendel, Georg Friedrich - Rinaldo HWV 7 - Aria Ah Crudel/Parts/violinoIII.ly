\version "2.19.82"

\include "../../stylesheet-parts.ily"
\include "../../commands.ily"

\include "../Notes/violinoIII.ily"

\include "../global.ily"

#(set-global-staff-size 20)

\header {
  instrument = \markup \scoreinstrument "Violino III"
 }

 \score {
  \violinoIII

 }
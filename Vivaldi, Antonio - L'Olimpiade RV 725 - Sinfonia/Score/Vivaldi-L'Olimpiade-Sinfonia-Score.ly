\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "../Notes/violino1.ily"
\include "../Notes/violino2.ily"
\include "../Notes/viola.ily"
\include "../Notes/basso.ily"

#(set-global-staff-size 14)


\book {
  \score {
    \new StaffGroup {
      <<
        \new Staff \with {
          instrumentName = "Violino I"
          midiInstrument = "violin"
          shortInstrumentName = "Vl. I"
        }\violinoI-partI

        \new Staff \with {
          instrumentName = "Violino II"
          midiInstrument = "violin"
          shortInstrumentName = "Vl. II"
        }\violinoII-partI

        \new Staff \with {
          instrumentName = "Viola"
          midiInstrument = "viola"
          shortInstrumentName = "Vla."
        }\viola-partI

        \new Staff \with {
          instrumentName = "Basso"
          midiInstrument = "cello"
          shortInstrumentName = "B."
        }\basso-partI
      >>

    }
    \header {
      piece = "Allegro"
    }
    \layout {}
    \midi {}
  }
  \pageBreak
  \score {
    \new StaffGroup {
      <<
        \new Staff \with {
          instrumentName = "Violini unisoni"
          midiInstrument = "violin"
          shortInstrumentName = "Vl."
        }\violinoI-partII

        \new Staff \with {
          instrumentName = "Viola"
          midiInstrument = "viola"
          shortInstrumentName = "Vla."
        }\viola-partII

        \new Staff \with {
          instrumentName = "Basso"
          midiInstrument = "cello"
          shortInstrumentName = "B."
        }\basso-partII
      >>
    }
    \header {
      piece = "Andante"
    }
    \layout {}
    \midi {}
  }
  \pageBreak
  \score {
    \new StaffGroup {
      <<
        \new Staff \with {
          instrumentName = "Violino I"
          midiInstrument = "violin"
          shortInstrumentName = "Vl. I"
        }\violinoI-partIII

        \new Staff \with {
          instrumentName = "Violino II"
          midiInstrument = "violin"
          shortInstrumentName = "Vl. II"
        }\violinoII-partIII

        \new Staff \with {
          instrumentName = "Viola"
          midiInstrument = "viola"
          shortInstrumentName = "Vla."
        }\viola-partIII

        \new Staff \with {
          instrumentName = "Basso"
          midiInstrument = "cello"
          shortInstrumentName = "B."
        }\basso-partIII
      >>
    }
    \header {
      piece = "Allegro"
    }
    \layout {\override Staff.TimeSignature.style = #'single-digit }
    \midi {}
  }
}
\version "2.19.82"

viola-partI = \relative c' {
  \clef alto \key c \major \time 4/4
  e8 e e e e\p e e e |  d\f d d d d\p d d d | c\f c c c c\p c c c |

  b16 b b b b b b b b b b b b b b b | c c c c c c c c g4 r\fermata | c16\pianol c c c c c c c c c c c c c c c |  c a a a a a a a d\pianol d d d d d d d |
  d d d d d d d d d\f b b b b b b b | g'8\p g g g aes aes aes aes | d, d d d g g g g |

  c, c c c f f f f | aes aes aes aes g16\f g g g d d b b | d d d d d d d d d d d d d d d d | fis fis fis fis fis fis fis fis d d d d fis fis fis fis |
  fis fis fis fis fis fis fis fis  d d d d d d d d | b b b b b b b b b\p b b b b b b b | a\f a a a a\p a a a g\f g g g g\p g g g |

  fis'\f fis fis fis fis fis fis fis fis fis fis fis fis fis fis fis | g g g g g g g g fis8 fis fis fis | fis\p fis dis dis b\f b b b | c\p c c c' fis,16 fis fis fis fis fis fis fis |
  fis fis fis fis fis fis dis dis b b b b b b b b | e,32[ e fis e] e[ e fis e] e[ e fis e] e[ e fis e]  b''8 b b b | c c c c fis, fis fis fis |

  b b b b e, e e e | a a a a dis, dis dis dis | g,8 g g g a a a a | g g g g a a a a | g g g g a8 a b b | e,4 r e'8 e e\p e | d\f d d\p d c\f c c\p c | d\f e' e e d d c c |
  b e\p e e d d c c | b32 g f e d c b a g16 b b b g b b b g g' e c | r2 c16\f c e e f f g g | c,4 r c16\f c e e f f g g | c,1\fermata \bar "|."
}

viola-partII = \relative c' {
  \clef alto \time 4/4 \key c \minor \partial 8
  es8 | es es f es d es d b' | b b b b g b b b | b b b b g es es es | e e e e e e e e | e e e e c aes aes aes |
  c c c c d d d d | d d d d c c c c | f d bes bes bes bes bes bes | bes[ bes] f'[ d] es es f d | es es es es d d es es |
  d d c c bes bes bes bes | bes bes c c bes bes c bes | g\f bes bes bes bes[ g'] es[ c] | f, bes c bes g4 r8 es'\p | es es f es d[ es] c[ f] |

  d8[ es] c[ f] d es es es | c c c c c c c c | d d d d d d d d | c c c c c c c c |
  d d d d d d d d | g g g g aes aes aes aes | bes bes bes bes c[ c] a[ a] | d, b c d es es d d | c[ c] d[ d] d[ g aes g] | es g g f es es d d | c c d\f d d g aes g | es1\fermata \bar "|."
}

viola-partIII = \relative c' {
  \clef alto \key c \major \time 3/8
  \repeat volta 2 {
    \partial 8
    e8 | d e e | b g g' | d b d | g,4 e'8 | f e e | f[ d] c | b d g, | g4 c'8\p | b c16 b c8 | g c16 b c8 | g g, c' | f, g c, | g g' c | f, g c, | g g' c, | g' g, g | g'16\f fis e d c b | a' g fis e d8 | g c, d | g,4 }
  \repeat volta 2 {
   d'8 | d4 a'8 | b a b | a4 a8 | d, a' b |

   a4 fis8 | d16\f d c b a g | b d c b a g | fis8 e d | b''8\p b b | b aes aes | aes ges ges | f f, f | c''16 b a g f e | a g f e d c | f8 g g, | c4
  }
}

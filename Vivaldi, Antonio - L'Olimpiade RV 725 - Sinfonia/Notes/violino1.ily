\version "2.19.82"
\include "../../commands.ily"

%f°1r
violinoI-partI = \relative c''{
  \clef treble \time 4/4 \key c \major
  c32[ c d c] c[ c d c] c[ c d c] c[ c d c] c\p[ c d c] c[ c d c] c[ c d c] c[ c d c]| g'\f[ g a g] g[ g a g] g[ g a g] g[ g a g] g\p[ g a g] g[ g a g] g[ g a g] g[ g a g]|
  c\f[ c d c] c[ c d c]c[ c d c] c[ c d c] c\p[ c d c] c[ c d c] c[ c d c] c[ c d c] |

  b8 g,, r16 g''16 d b g,4 r16 f''16 d b | g,4 r16 e'' c g g,4 r\fermata | c'32\pianol [ c d c] c[ c d c] c[ c d c] c[ c d c] bes[ bes c bes] bes[ bes c bes] bes[ bes c bes] bes[ bes c bes] | a4 f32[ f g f] f[ f g f] d'[\pianol d e d] d[ d e d] d[ d e d] d[ d e d]|

  %f°1v
  c[ c d c] c[ c d c] c[ c d c] c[ c d c] g\f[ g a g] g[ g a g] g[ g a g] g[ g a g] | r2 \tuplet 3/2 8 {c'16\p[( bes) c] c16[( bes) c] c16[( bes) c] c16[( bes) c] } | c16 c aes aes f f d d \tuplet 3/2 8 {bes'16([ aes) bes] bes16([ aes) bes] bes16([ aes) bes] bes16([ aes) bes]}

  bes16 bes g g es es c c \tuplet 3/2 8 {aes'16[( g) aes] aes[( g) aes] aes16[( g) aes] aes[( g) aes]} | \tuplet 3/2 8 {fis16[( e) fis] fis16[( e) fis] fis16[( e) fis] fis16[( e) fis]} g32\f[ g a g] d[ d e d] b[ b c b] g[ g a g] | d16 a'' fis d d, b'' g d d, a'' fis d d, b'' g d | d, c' \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] b16 g'32( fis e d c b) a16 fis d fis' |

  \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] b16 g'32( fis e d c b) a16 fis d fis' | g,32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g32\p[ g a g] g32[ g a g] g32[ g a g] g32[ g a g]| d'\f[ d e d] d[ d e d] d\p[ d e d] d[ d e d]

  g\f[ g a g] g[ g a g] g\p[ g a g] g[ g a g] | d'8\f d, r16 d' a fis d,4 r16 c'' a fis | d,4 r16 b''16 g d d, a' d fis a fis d a | dis,\p fis b dis fis a b fis e,\f g b e g b g e | a,,\p c e a c e a e

  \appoggiatura e8 dis16\f[(cis32 b)] \appoggiatura e8 dis16[(cis32 b)] \appoggiatura e8 dis16[(cis32 b)] \appoggiatura e8 dis16[(cis32 b)] | \appoggiatura b'8 a16[ g32( fis)] \appoggiatura b8 a16[ g32( fis)] \appoggiatura b8 a16[ g32( fis)] \appoggiatura b8 a16[ g32( fis)] g[ g b g] e[ e g e] b[ b e b] g[ g b g] | e32[ e fis e] e32[ e fis e] e32[ e fis e] e32[ e fis e] r2 | \tuplet 3/2 8 {e''16[( dis) e] e16[( dis) e] e16[( dis) e] e16[( dis) e]}

  e16 e c c a a fis fis | \tuplet 3/2 8 {d'16[( c) d] d16[( c) d] d16[( c) d] d16[( c) d]} d d b b g g e e | \tuplet 3/2 8 {c'16[( b) c] c16[( b) c] c16[( b) c] c16[( b) c]} c c a a fis fis dis dis | g,,32\f([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] |  g,32([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] | << { b4} \\ {<g, e'>16 e'' b g} >> g, e''  b g a, e'' c a b, dis' b b, |

  e4 r c'32[ c d c] c[ c d c] c32\p[ c d c] c[ c d c] | g'\f[ g a g] g[ g a g] g\p[ g a g] g[ g a g] c32\f[ c d c] c[ c d c] c32\p[ c d c] c[ c d c] | b16\f[ g] \appoggiatura a8 g16[ f32( g)] e16 g \appoggiatura a8 g16[ f32( g)] d16 g \appoggiatura a8 g16[ f32( g)] c,16 g' \appoggiatura a8 g16[ f32( g)] |

  b,16\p g' \appoggiatura a8 g16[ f32( g)] e16 g \appoggiatura a8 g16[ f32( g)] d16 g \appoggiatura a8 g16[ f32( g)] c,16 g' \appoggiatura a8 g16[ f32( g)] | b,32\f g f e d c b a g16 g'' d  b g, f'' d b g, e'' c e, | \tuplet 3/2 8 {g'16\p[( f g)] g([ f g)] g([ f g)] g([ f g)]} e32[\f c b c] e,[ c' b c] f,[ c' b c] g[ c b c] | \tuplet 3/2 8 {g'16\p[( f g)] g([ f g)] g([ f g)] g([ f g)]} e32[\f c b c] e,[ c' b c] f,[ c' b c] g[ c b c] | c,1 \bar "|."
}

violinoI-partII = \relative c'' {
  \clef treble \time 4/4 \key c \minor \partial 8
  g'8 | g8.\trill f32 g aes8 c, b c f8.( g32 aes) | aes4( g16) f32 es f16 d es8 d f8.(-\markup \italic "sempre piano" g32 aes) | aes4( g16) f32 es f16 d es8 c r g' | bes16-.( aes-. g-. f-. e-. des-. c-. bes-. ) c,8 \tuplet 3/2 8 {bes''16([ g e] bes'[ g e] bes'[ g e])} | des'4~des16 c32 bes aes16 g aes f c aes f8 f' |
  aes16-.( g-. f-. es-. d-. c-. bes-. aes-.) bes,8 f'' f~f32 es( f g) | aes2( a) |  bes16-. f-. g-. d-. es-. bes-. c-. g-. aes-. f-. g-. es-. f-. d-. es-. c-. | bes8 r aes''8.\trill g32 f g8 r aes8.\trill g32 f | g16.([ bes32)] bes,16.([ aes'32)] g4\trill f16.([ bes32)] bes,16.([ aes'32)] g4\trill |
  f4~f8~f32[ f( g aes] bes16-.)( aes-. g-. f-. g-. f-. es-. d-. | es-. d-. c-. bes-. aes-. g-. f-. es-.) bes8 es16. f32 f4\trill | es8 r bes''32\f([ aes) g( f)] g([ f) es( d)] es[( d) c( bes)] c[( bes) aes( g)] aes([ g) f( es)] f([ es) d( c)] | bes8 es16. f32 f4\trill es r8 g'\p | g8.\trill f32 g aes8 c, b-. c-. \tuplet 3/2 8 {aes'16([ f) g] aes([ f) d]}

  b8-. c-. \tuplet 3/2 8 {aes'16[( f) g] aes([ f) d]} b8-. c-. r g' |  g16( c, es, c' g'8) r aes16( c, f, c' aes'8) r | a16( d, fis, d' a'8) r b16( d, g, d' b'8) r | g16[ g32( f)] es16[ es32( d)] c16[ c32( bes)] aes16[ aes32( g)] f16[ f32( e)] f16[ f32( e)] f4\trill |
  \tuplet 3/2 8 {aes'16[ aes( g)] f[ f( es)] d[ d( c)] b[ b( a)] g[ g( fis)] g[ g( fis)]} g8 r | c16[ c32( b)] c16[ c32( b)] c8 g' des16[ des32( c)] des16[ des32( c)] des8 aes' | e16[ bes'32( g)] e16[ bes'32( g)] e16[ bes'32( g)] e16[ bes'32( g)] aes16[ aes32( g)] f16[ f32( es)] d16[ d32( c)] b16[ b32( a)] |
  g4 r \tuplet 3/2 8 {c16[( es d] c[ b c]) c16[( es d] c[ b c])} | \tuplet 3/2 8 {c16[( es d] c[ b? c]) c16[( es d] c[ b? a?])} g8 c16. d32 d4\trill | c2 \tuplet 3/2 8 {g'16[( es d] c[ b c]) g'16[( es d] c[ b c])} | \tuplet 3/2 8 {g'16[( es d] c[ b c]) g'16[(\f es d] c[ b a])} g8 c16. d32 d4\trill | c1\fermata \bar "|."
}

violinoI-partIII = \relative c'' {
  \clef treble \key c \major
  \repeat volta 2 {
    \time 3/8
    \partial 8 g'8 | b, c g' | f\trill e c | b16 c d f, e d | e8 c c' | \grace {b16( c)} d8 c c | \grace {b16( c)} d8 c e, | d16 e f d e c | g4 g''8\p | aes g4 | f8 es4 | d16 es f8 g | aes( b,) c | d16 es f8 g | aes( b,) c | d16 es f8 es | es\trill d g,, | g''16\f fis e d c b | a' g fis e d8 | g c, d | g,4
  }
  \repeat volta 2 {
  d'8 | b16 c d8 e |  d8 fis( g) | a,16 b c8 d | b fis'( g) |

  a,16 b c8 d | b16\f d c b a g | b d c b a g | fis8 e d | g'8\p( aes) es | f( g) d | es( f) c | b a16 b g8 | c16 b a g f e | a g f e d c | f8 g g, | c4
  }
}
\version "2.19.82"

basso-partI = \relative c {\clef bass \key c \major \time 4/4

 c8 c c c c\p c c c | b\f b b b b\p b b b | a\f a a a a\p a a a |
 g32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] | g32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g4 r\fermata | e'16\pianol \senzacemb e e e e e e e e e e e e e e e | f32[\tutti f g f] f[ f g f] f[ f g f] f[ f g f] fis16\senzacemb fis fis fis fis fis fis fis |

 fis fis fis fis fis fis fis fis g32[\f \tutti g a g] g[ g a g] g[ g a g] g[ g a g] | g,32[ g aes g] g8 r4 g32[ g aes g] g8 r4 | g32[ g aes g] g8 r4  g32[ g aes g] g8 r4 |
  g32[ g aes g] g8 r4  g32[ g aes g] g8 r4 | g32[ g aes g] g8 r4 g16 g g g g g g g | d'32[ d e d] d[ d e d] d[ d e d] d[ d e d] d[ d e d] d[ d e d] d[ d e d] d[ d e d] | d16 d d d d d d d g, g g g d' d d d |

  d d d d d d d d g, g g g d' d d d | g8 g g g g\p g g g | fis\f fis fis\p fis e\f e e\p e | d32[\f d e d] d32[ d e d]d32[ d e d]d32[ d e d]d32[ d e d]d32[ d e d]d32[ d e d]d32[ d e d] | d32[ d e d]d32[ d e d]d32[ d e d]d32[ d e d] d8 d d d | b\p b b b e\f e e e | a,\p a a a b16 b b b b b b b |

  b b b b b b b b e e e e e e e e | e32[ e fis e] e32[ e fis e] e32[ e fis e] e32[ e fis e] b32[ b c b] b8 r4 | b32[ b c b] b8 r4 b32[ b c b] b8 r4 |
  b32[ b c b] b8 r4 b32[ b c b] b8 r4 | b32[ b c b] b8 r4 b32[ b c b] b8 r4 | e8 e e e e e e e | e e e e e e e e | e e g, g a a b b |

  e,4 r c'8 c c\p c | b\f b b\p b a\f a a\p a | g\f c' c c b b a a |
  g c\p c c b b a a | g32[ g f e] d[ c b a] g[ g a g] g[ g a g] g[ g a g] g[ g a g] g[ g a g] g[ g a g]| r2 c16\f c e e f f g g | c,4 r c16\f c e e f f g g | c,1\fermata \bar "|."
}

basso-partII = \relative c { \clef bass \key c \minor \time 4/4 \partial 8
 r8\senzacemb | c c c c g' c, g g | g g g g c g g g | g g g g c c c c | c c c c c c c c | c c c c f f f f | f f f f bes, bes bes bes | bes bes bes bes f' f es es | d bes bes bes bes bes bes bes | bes bes bes bes bes bes bes bes | bes bes a a bes bes a a |
 bes bes aes aes g g g g | g g aes aes bes g aes bes | es\f g, g g g[ es'] c[ a] | bes g aes bes es,4 r8 c'\p | c c c c  g'[ c,] f[ f] |

 g[ c,] f[ f] g c, c c | es es es es f f f f | fis fis fis fis g g g g | es es es es aes, aes aes aes |
 f' f f f g g g f | es es es es f f f f | g g g g f f fis fis |
 g g a b c c b b | aes aes g fis g es f g | c, es' es d c c bes bes | aes aes g\f fis g es f g | c,1\fermata \bar "|."

}

basso-partIII = \relative c {\clef bass \key c \major \time 3/8
 \repeat volta 2 { \partial 8
c8 | g' c,16 d e c | g8 c16 d e f | g8 g, b | c16 b c d e c | g'8 c,16 d e c | g'8 c,16 d e c | g'8 b, c | g4 r8 | R4. | R4. | R4. | R4. |
R4. | R4. | R4. | R4. | g'16\f fis e d c b | a' g fis e d8 | g c, d | g,4  }
 \repeat volta 2 {
  g'8 | g, g' fis | g d g, | d d' fis | g d g, |

  d d' fis | g16\f d' c b a g | b d c b a g | fis8 e d | R4. | R4. | R4. | R4. |c'16 b a g f e | a g f e d c | f8 g g, | c4
 }
}
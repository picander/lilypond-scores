\version "2.19.82"
\include "../Notes/violino1.ily"

violinoII-partI = \relative c''{
  \clef treble \time 4/4 \key c \major
  c32[ c d c] c[ c d c] c[ c d c] c[ c d c] c\p[ c d c] c[ c d c] c[ c d c] c[ c d c]| g'\f[ g a g] g[ g a g] g[ g a g] g[ g a g] g\p[ g a g] g[ g a g] g[ g a g] g[ g a g]|
  c\f[ c d c] c[ c d c]c[ c d c] c[ c d c] c\p[ c d c] c[ c d c] c[ c d c] c[ c d c] |

  b8 g,, r16  d'' b g g,4 r16 d'' b g | g,4 r16 c' g e g,4 r\fermata | g'32[\pianol g g g] g[ g g g] g[ g g g] g[ g g g] g[ g a g] g[ g a g] g[ g a g] g[ g a g] | f4 f32[ f g f] f[ f g f] a[\pianol a a a] a[ a a a] a[ a a a] a[ a a a] |

  a32[ a b a]  a32[ a b a]  a32[ a b a]  a32[ a b a] g\f[ g a g] g[ g a g] g[ g a g] g[ g a g] | \tuplet 3/2 8 {g'16[(\p fis) g] g16[( fis) g] g16[( fis) g] g16[( fis) g]} g16 g es es c c aes aes | \tuplet 3/2 8 {f'16([ e) f] f16([ e) f] f16([ e) f] f16([ e) f] } f16 f d d bes bes g g |

  \slurDashed \tuplet 3/2 8 {es'[( d) es] es[( d) es] es[( d) es] es[( d) es]} \slurSolid es16 es c c aes aes f f | c' c c c c c c c b32[\f b c b] b32[ b c b] g[ g a g] d[ d e d] | d16 fis' d a d, g' d b d, fis' d a d, g' d b | d, c' \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] b16 g'32( fis e d c b) a16 fis d fis' |

  \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] \appoggiatura d8 c16[ b32( a)] b16 g'32( fis e d c b) a16 fis d fis' | g,32[ g a g] g32[ g a g] g32[ g a g] g32[ g a g] g32\p[ g a g] g32[ g a g] g32[ g a g] g32[ g a g]| d'\f[ d e d] d[ d e d] d\p[ d e d] d[ d e d]

  g\f[ g a g] g[ g a g] g\p[ g a g] g[ g a g] | d'8\f d, r16 a' fis d d,4 r16 a'' fis d | d,4 r16 g' d b d, a' d fis a fis d a | dis,\p fis b dis fis a b fis e,\f g b e g b g e | a,,\p c e a c e a e

  \appoggiatura e8 dis16\f[(cis32 b)] \appoggiatura e8 dis16[(cis32 b)] \appoggiatura e8 dis16[(cis32 b)] \appoggiatura e8 dis16[(cis32 b)] | \appoggiatura g'8 fis16[ e32( d)] \appoggiatura g8 fis16[ e32( d)] \appoggiatura g8 fis16[ e32( d)] \appoggiatura g8 fis16[ e32( d)] e32[ e g e] b[ b e b] g[ g b g] e[ e g e] | e32[ e fis e] e32[ e fis e] e32[ e fis e] e32[ e fis e] \tuplet 3/2 8 {b''16[( a) b] b[(a) b] b[(a) b] b[(a) b]} | b16 b g g e e c c \tuplet 3/2 8 {a'16[( gis) a] a[( gis) a] a[( gis) a] a[( gis) a]} | a16 a fis fis d d b b \slurDashed \tuplet 3/2 8 {g'16[( fis) g] g[( fis) g] g[( fis) g] g[( fis) g]} \slurSolid | g16 g e e c c a a a' a fis fis dis dis b b |g,32\f([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] |  g,32([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] g,32([ e' b') b] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] a,32[( e' c') c] | << { b4} \\ {<g, e'>16 e'' b g} >> g, e''  b g a, e'' c a b, d' b b, |

  e4 r c'32[ c d c] c[ c d c] c32\p[ c d c] c[ c d c] | g'\f[ g a g] g[ g a g] g\p[ g a g] g[ g a g] c32\f[ c d c] c[ c d c] c32\p[ c d c] c[ c d c] | b16\f[ g] \appoggiatura a8 g16[ f32( g)] e16 g \appoggiatura a8 g16[ f32( g)] d16 g \appoggiatura a8 g16[ f32( g)] c,16 g' \appoggiatura a8 g16[ f32( g)] |

  b,16\p g' \appoggiatura a8 g16[ f32( g)] e16 g \appoggiatura a8 g16[ f32( g)] d16 g \appoggiatura a8 g16[ f32( g)] c,16 g' \appoggiatura a8 g16[ f32( g)] | b,32\f g f e d c b a g16 d'' b g g, d'' b g g, c' g e | \tuplet 3/2 8 {e'16\p[( d) e] e[( d) e] e[( d) e] e[( d) e]} e32[\f c b c] e,[ c' b c] f,[ c' b c] g[ c b c] | \tuplet 3/2 8 {e16\p[( d) e] e[( d) e] e[( d) e] e[( d) e]} e32[\f c b c] e,[ c' b c] f,[ c' b c] g[ c b c] | c,1 \bar "|."
}

violinoII-partII = \violinoI-partII

violinoII-partIII = \relative c'' {
  \clef treble \key c \major \time 3/8
  \repeat volta 2 {
    \partial 8
    g'8 | b, c g' | f\trill e c | b16 c d f, e d | e8 c c' | \grace {b16( c)} d8 c c | \grace {b16( c)} d8 c e, | d16 e f d e c | g4 es''8\p | f es4 | d8 c4 | b16 c d8 es | f( d) es | b16 c d8 es | f( d) es | b16 c d8 c | c b g, | g''16\f fis e d c b | a' g fis e d8 | g c, d | g,4
  }
  \repeat volta 2 {
   b8 | g16 a b8 c | b fis'( g) | fis,16 g a8 a | g fis'( g) |

   fis,16 g a8 a | g16\f d' c b a g | b d c b a g | fis8 e d | es'8 es aes | d, d g | c, c f | d c16 d b8 | c16 b a g f e | a g f e d c | f8 g g, | c4 }
}
\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"
\include "stylesheet-parts.ily"

\include "../Notes/violino1.ily"

\book {
  \header {
    instrument = \markup \scoreinstrument "Violino I"
  }
  \score {
    \new Staff{
      <<
        \mark \header-partI
        \new Voice{\violinoI-partI}
      >>
    }
  }
  \pageBreak
  \score {
    \new Staff{
      <<
        \mark \header-partII
        \new Voice{\violinoI-partII}
      >>
    }
  }
  \score {
    \new Staff{
      <<
        \mark \header-partIII
        \new Voice{\violinoI-partIII}
      >>
    }
  }
}

\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"
\include "stylesheet-parts.ily"

\include "../Notes/viola.ily"

\book {
  \header {
    instrument = \markup \scoreinstrument "Viola"
  }
  \score {
    \new Staff{
      <<
        \mark \header-partI
        \new Voice{\viola-partI}
      >>
    }
  }
  \pageBreak
  \score {
    \new Staff{
      <<
        \mark \header-partII
        \new Voice{\viola-partII}
      >>
    }
  }
  \markup {\vspace #1 }
  \score {
    \new Staff{
      <<
        \mark \header-partIII
        \new Voice{\viola-partIII}
      >>
    }
  }
}

\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"
\include "stylesheet-parts.ily"

\include "../Notes/basso.ily"

\paper {
markup-system-spacing.basic-distance = #5
}
\book {
  \header {
    instrument = \markup \scoreinstrument "Basso"
  }
\markup {\vspace #1 }
  \score {
    \new Staff{
      <<
        \mark \header-partI
        \new Voice{\basso-partI}
      >>
    }
  }
\pageBreak
  \score {
    \new Staff{
      <<
        \mark \header-partII
        \new Voice{\basso-partII}
      >>
    }
  }
\markup {}
  \score {
    \new Staff{
      <<
        \mark \header-partIII
        \new Voice{\basso-partIII}
      >>
    }
  }
}
\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"
\include "stylesheet-parts.ily"

\include "../Notes/violino2.ily"

\book {
  \header {
    instrument = \markup \scoreinstrument "Violino II"
  }
  \score {
    \new Staff{
      <<
        \mark \header-partI
        \new Voice{\violinoII-partI}
      >>
    }
  }
  \pageBreak
  \score {
    \new Staff{
      <<
        \mark \header-partII
        \new Voice{\violinoII-partII}
      >>
    }
  }
  \score {
    \new Staff{
      <<
        \mark \header-partIII
        \new Voice{\violinoII-partIII}
      >>
    }
  }
}

\version "2.19.82"

\header {
  title = \markup \scoretitle "L'Olimpiade"
  subtitle = \markup \scoresubtitle "Sinfonia"
  subsubtitle = \markup \scoresubsubtitle "Ms. I-Tn: Foà 39, f.1v-5r"
  composer = \markup \scorecomposer "Antonio Vivaldi"

  tagline = \markup {\override #'(font-name . "Perpetua") "Typesetting by Picander with Lilypond. License CC BY-NC 4.0." }
}
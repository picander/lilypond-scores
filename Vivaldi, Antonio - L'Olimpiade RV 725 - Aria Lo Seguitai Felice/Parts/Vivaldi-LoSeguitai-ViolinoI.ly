\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../Notes/violinoI.ily"
\include "../../stylesheet-parts.ily"

#(set-global-staff-size 17)


\header {
  instrument = \markup \scoreinstrument "Violino I"
}

\score {
 \violinoI
}
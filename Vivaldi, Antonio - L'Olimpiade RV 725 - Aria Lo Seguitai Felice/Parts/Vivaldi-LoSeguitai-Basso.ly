\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../../stylesheet-parts.ily"

\include "../Notes/basso.ily"

#(set-global-staff-size 20)


\header {
  instrument = \markup \scoreinstrument "Basso"
}

\score {
 \basso
}
\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../Notes/viola.ily"
\include "../../stylesheet-parts.ily"

#(set-global-staff-size 20)


\header {
  instrument = \markup \scoreinstrument "Viola"
}

\score {
 \viola
}
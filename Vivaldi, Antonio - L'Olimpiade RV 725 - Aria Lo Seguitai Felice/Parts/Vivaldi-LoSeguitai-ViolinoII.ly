\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"
\include "../Notes/violinoII.ily"
\include "../../stylesheet-parts.ily"

#(set-global-staff-size 18)


\header {
  instrument = \markup \scoreinstrument "Violino II"
}

\score {
 \violinoII
}
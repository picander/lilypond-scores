\version "2.19.82"

megaclenotes = \relative c'{
 \clef treble
 \key bes \major
 \time 4/4
 \autoBeamOff
 \partial 8
 \tempo "Allegro non molto"
  r8 | R1 | R1 | R1 |
  R1 | R1 |
  R1 | R1 | R1 |

  r2 r4 r8 f | bes bes bes\trill a16[ bes] c[ bes] c8 r f, | bes8 bes f'16[ es] d[ c] bes[ a] g8 r d' | f,16[ g] a bes c[ d] es[ f] d[ c] bes8 r f |
  bes f d' f, g es r g | c e c g a f r a |
  d8[ a] fis[ d] \tuplet 3/2 8 { bes'16[ d c] } bes8 \tuplet 3/2 8 { r16 g[ a] bes[ c d]} | \tuplet 3/2 { es16[ f g]} g8~\tuplet 3/2 8 { g16[ f es] d[ c bes]} a8[ a~] \tuplet 3/2 8{a16[ c bes] a[ g f]} | \tuplet 3/2 { d'16[ es f]} f8~ \tuplet 3/2 8 { f16[ es d] c[ bes a]} g8[ g~] \tuplet 3/2 8 { g16[ es f] g[ a bes]} |

\tuplet 3/2 { c16[ d es] } es8~\tuplet 3/2 8 { es16[ d c] bes[ a g]} fis8 d r d | g4\trill a\trill bes\trill c\trill | d8 d, r d g g g a |
bes8 g16 a bes8 c d bes16 c d8 d | \tuplet 3/2 8 { g16[ d c] bes[ a g] g'[ d c] bes[ a g] } c,4 d | g, r r2 | R1 | r4 r8 d' g g g16[ a] bes[ c] | d8 d, r d g g g16[ a] bes[ c] |

d8 d, r d' c2 | bes a4. d8 | g,2 fis4 r8 d |
bes' d bes f g es r g | c e c g a f r a |
d,8[ fis] a[ d] \tuplet 3/2 8 { bes16[ d c] bes[ a g] bes[ d c] bes[ a g] } | es'4~\tuplet 3/2 8 { es16[ d c] bes[ a g] a[ c bes] a[ g f!] a[ c bes] a[ g f] }

d'4~\tuplet 3/2 8 { d16[ c bes] a[ g f] g[ bes a] g[ f es] g[ bes a] g[ f es] } | \tuplet 3/2 8 { c'16[ d es] } es8~\tuplet 3/2 8 { es16[ d c] bes[ a g] } a8 f \tuplet 3/2 8 { r16 f[ g] a[ g] f } |
\tuplet 3/2 8 { bes16[ f g] a[ g f] c'[ f, g] a[ g f] d'[ f, g] a[ g f] es'[ f, g] a[ g f] } | f'8[ es16 d c bes] a g f4 r8 f | bes bes bes bes bes[ d16 c] bes8  bes |
bes8 d16 c bes8 bes f' f, r a | bes bes bes c d bes16 c d8 es |

f8 bes,16 c d8 es \tuplet 3/2 8 { f16[ bes, c] d[ c bes] f'[ bes, c] d[ c bes] } | es,4 f bes8 f' d bes | es,4 f bes, r |
R1 | R1 | R1 |
r4 r8 d' d g, g aes | aes4 g d' f, | r8 aes g16[ f] es d es8 c r4 |

g''16[ f] es d c[ bes] aes[ g] aes8 f r4 | f'16[ es] d c bes[ aes] g[ f] g8 es r g |
es'16[ d] c bes aes[ g] f es f4 r | c'8 g16 g g8 c d g, g d' | es16[ d] c8 r c f, aes g16[ f] es d |
c4 c'~c8 fis,16 fis fis8 c' | c4\trill bes d16[ c] bes a g[ f] es d | es8 c r4 es'16[ d] c16 bes c[ bes] a g |

fis8 d r4 bes'8 g16 g g8 bes8 | c a a c d d, r d | g es' d16[ c] bes a g2\fermata \bar "|."
}

megaclelyrics = \lyricmode {
  Lo se -- gui -- tai fe -- li -- ce quand' e -- ra~il ciel se -- re -- no, quand' e -- ra il ciel se -- re -- no, nel --
  le tem -- pe -- ste~in se -- no, nel -- le tem -- pe -- ste~in se -- no vo'
  se -- gui -- tar__ - - - - - - - - - - - - - - - - - lo, vo' se -- gui -- tar -- - - lo, nel -- le tem -- pe -- ste~in
  se -- no vo' se -- gui -- tar -- lo, vo' se -- gui -- tar -- - - - - lo~an -- cor.

  Lo se -- gui -- tai fe -- li -- ce quand' e -- ra~il ciel se -- re -- no, il ciel se -- re -- - - no, nel -- le tem -- pe -- ste~in se -- no, nel -- le tem -- pe -- ste~in se -- no,  vo'
  se -- gui -- tar -- - - - - - - - - - - - - - - - - - - - lo, vo' se -- gui  -- tar -- - - - - - - - - lo an -- cor.
  Lo se -- gui -- tai fe -- li -- ce quand' e -- ra il ciel se -- re -- no, nel -- le tem -- pe -- ste~in se -- no vo' se -- gui -- tarv -- lo, vo' se -- gui -- tar -- - - - - lo~an -- cor, vo' se -- gui -- tar -- lo~an -- cor.
  Co -- me dell' o -- ro~il fuo -- co scuo -- pre le mas -- se im -- pu -- re,

  scuo -- pro -- no le sven -- tu -- re, scuo -- pro -- no le sven -- tu -- re de'
  fal -- si a -- mi -- ci il cor, scuo -- pro -- no le sven -- tu --re, le sven -- tu -- re de' fal -- si~a -- mi -- ci il
  cor. Co -- me dell' o -- ro~il fuo -- co scuo -- pre le mas -- se im -- pu -- re, scuo -- pre le mas -- se im -- pu -- re, scuo -- pro -- no le sven -- tu -- re, le sven -- tu -- re de' fal -- si~a -- mi -- ci il cor.
}
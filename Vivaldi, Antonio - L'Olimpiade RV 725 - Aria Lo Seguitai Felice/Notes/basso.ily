\version "2.19.82"

basso = \relative c'{
 \clef bass
 \key bes \major
 \time 4/4
 \partial 8 \tempo "Allegro non molto"
  r8 | bes2\f a | g f | es4. d16\p c bes8 bes bes bes |
  d16\f d d d d d d d es es es es es es es es | e e e e e e e e f f f f f f f f |
  bes8 bes a a g g f f | es es f f bes,4 r |  bes'8\p bes a a g g f f |

  es\f es f f bes,4 r | bes'2 a | g f | es4. d16 c bes8 bes bes bes |
  d16 d d d d d d d es es es es es es es es | e e e e e e e e f f f f f f f f |
  fis fis fis fis fis fis fis fis g8 g g g | c, c c c f f f f | bes, bes bes bes es es es es |

  es es es es d d d d | d d d d d d d d | d d d fis  g g f f |
  es es d d c c bes bes | a\f a g g c c d d | g,16 g' g g g f es d c c c c c d es c |
  d16 d d d d d e fis \tuplet 3/2 8 { g[ d c] bes[ a g] } c8 d | g,4 r g'2 | f  es |

  d1~| d~| d2 d8 d d d |
  d16 d d d d d d d es es es es es es es es | e e e e e e e e f f f f f f f f |
  fis16 fis fis fis fis fis fis fis g8 g g g | c, c c c f! f f f |

  bes, bes bes bes es es es es | es es es es f f f f |
  f f f f f f f f |\clef tenor f'\f[ es16 d c bes a g] f8 f f f  | \clef bass bes2\p a |
  g f | bes8 bes bes bes g g f f |

  es8 es d d c\f c bes bes | es es f f bes, f'' d bes | es, es f f bes, bes' bes bes |
  c c ,f f bes bes es, es | f f f f f f f f | f'16 f d d bes bes d, d es8 es f f |
  bes,4\fermata \fine r8 b\p b b b b | b b b b b b b b | b b b b c c c c |

  \clef tenor g''16\f f es d c bes aes g \clef bass aes f g aes aes, f' g aes \clef tenor f' es d c bes aes g f \clef bass g es f g g, es' f g |
   es'16 d c bes aes g f es g8 g g g | g g g g g g g g | g g g g g g g g |
   c,8 fis fis fis fis fis fis fis | g g, g g d''16 c bes a g f es d | es es f g c, es f g es' d c bes c bes a g |

   d8 d d d d d d d | d d d d d d d d | d d d d g,2\fermata \bar "|."\dc
}
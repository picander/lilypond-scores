\version "2.19.82"

violinoII = \relative c'{
 \clef treble
 \key bes \major
 \time 4/4
 \partial 8
  \tempo "Allegro non molto"
 f8\p | f f f f f f f f | f f es es c c c c | f16-. g-. a-. bes-. c-. d-. es-. f-. f8 bes,, bes bes |
 \tuplet 3/2 8 {bes''16\f[ f d] bes[ d f] bes[ bes, bes] bes[ bes bes] g[ es bes] g[ bes es] } g8 r | \tuplet 3/2 8 {c'16[ g e] c[ e g] c[ c, c] c[ c c] a[ f c] a[ c f]} a8 r |
 \tuplet 3/2 8 { bes'16[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes]} | es,8 es f f bes,4 r | \tuplet 3/2 8 { bes''16\p[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes]} | es,8\f es f f bes,4 r8 f'\p | f f f f f f f f | f f es es c c c c | f16-. g-. a-. bes-. c-. d-. es-. f-. f8 bes,, bes bes |
\tuplet 3/2 8 {bes''16[ f d] bes[ d f] bes[ bes, bes] bes[ bes bes] g[ es bes] g[ bes es] } g8 r | \tuplet 3/2 8 {c'16[ g e] c[ e g] c[ c, c] c[ c c] a[ f c] a[ c f]} a8 r | \tuplet 3/2 8 {d'16[ a fis] d[ fis a] d[ d, d] d[ d d]} r2 | es2~es16 c a f a,8 r | d'2~d16 bes g es g,8 r |

c'2~c16 a fis d a8 fis' | g4( a) bes( c) | bes8 a g fis \tuplet 3/2 8 {g'16[ d c] bes[ a g] g'[ d c] bes[ a g]} | \tuplet 3/2 8 { g'16[ d c] bes[ a g] g'16[ d c] bes[ a g] g'16[ d c] bes[ a g] g'16[ d c] bes[ a g]} | \tuplet 3/2 8 { g'16[\f d c] bes[ a g] g'16[ d c] bes[ a g] } c,8 c d d | \tuplet 3/2 8 { g16[ bes c] d[ e fis] } g8 g,, \tuplet 3/2 8 { es'!16[  f! g] a[ bes c] c[ d es] es[ f g] } |
\tuplet 3/2 8 {fis16 a g] fis[ e d] c[ d e] d,[ e fis] g[ d c] bes[ a g]} c8 d | g,4 r8 d'\p d d d d | d d d d es es es c' |

a8 a a a a16-. a-. bes-. a-. a-. a-. bes-. a-. | g-. g-. a-. g-. g-. g-. a-. g-. fis-. fis-. g-. fis f-. f-. g-. f-. | es-. es-. f-. es-. e-. e-. f-. e-. a8 d, d d |
\tuplet 3/2 8 { bes''16[ f d] bes[ d f] bes[ bes, bes] bes[ bes bes] g[ es bes] g[ bes es] } g8 r | \tuplet 3/2 8 { c'16[ g e] c[ e g] c[ c, c] c[ c c] a[ f c] a[ c f]} a8 r |
\tuplet 3/2 8 { d'16[ a fis] d[ fis a] d[ d, d] d[ d d]} g2~| \tuplet 3/2 8 { g16[ g f!] es[ d c] } g'4 f2~|
\tuplet 3/2 8 { f16[ f es] d[ c bes] } f'4 es2~| \tuplet 3/2 8 { es16[ es d] c[ d es] } es4 \tuplet 3/2 8 { c16[ f, g] a[ g f] } c'8 a |
bes4( c) d( es) | f8\f[ es16 d c bes a g] \tuplet 3/2 8 { f16[ f g] a[ g f] } c'8 r | bes16\pp bes c bes bes bes c bes bes bes c bes bes bes c bes |
bes bes c bes bes bes c bes a a bes a a a bes a | \tuplet 3/2 8 { bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes]  } |

\tuplet 3/2 8 { bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[\f  f es] d[ c bes] bes'[ f es] d[ c bes]  } | es,8 es f f bes, f'16 f d d bes bes | es8 es f f \tuplet 3/2 8 { bes,16[ d' es] f[ es d] bes'[ a g] f[ es d] } |
\tuplet 3/2 8 { es16[ c d] es[ d c] a'[ g f] es[ d c] d[ bes c] d[ c bes] g'[ f es] d[ c bes] | a[ f g] a[ g f] bes[ f g] a[ g f] c'[ f, g] a[ g f] d'[ f, g] a[ g f] } | f'16 f d d bes bes d, d es8 es f f |
bes,4\fermata \fine r8 g'\p g d d f | f f d d f f f f | f f f f es es es es |

g'16\f f es d c bes aes g aes f g aes aes, f' g aes | f' es d c bes aes g f g es f g g, es' f g |
es'16 d c bes aes g f es f b c d g,, b' c d | g,, c' d es  g,, c' d es g,, d'' es f g,, d'' es f | g,, es'' f g g,, es'' f g g,,8 aes' g16 f es d |
c8 c' c c c c c c | c c bes bes d16 c bes a g f es d | es es f g c, es f g es' d c bes c bes a g |

d fis g a d, fis g a d, g a bes d, g a bes | d, a' bes c d, a' bes c d, bes' c d d, bes' c d | d,8 es' d16 c bes a g2\fermata \bar "|."\dc
}
\version "2.19.82"

viola = \relative c{
 \clef alto
 \key bes \major
 \time 4/4
 \partial 8
\tempo "Allegro non molto"
  d'8\p | d d d d c c c c | d d bes bes a a a a | c c c c d bes bes bes |
  f'16\f f f f f f f f bes, bes bes bes bes bes bes bes | g' g g g g g g g c, c c c c c c c |
  d'8 d c c bes bes a a | es es f f bes,4 r | d'8\p d c c bes bes a a |

  es\f es f f bes,4 r8 d\p | d d d d c c c c | d d bes bes a a a a | c c c c bes bes bes bes |
  f'16 f f f f f f f bes, bes bes bes bes bes bes bes | g' g g g g g g g c, c c c c c c c |
  a' a a a a a a a d,8 d d g | g g g es c c c f | f f f d bes bes bes es |

  g, g g c d d d d | d d d d d d d d | d d d d bes' bes a a |
  g g f f es es d d | c\f c bes bes c c d d | bes16 bes bes bes bes a g fis g es' es es es f! g es |
  a, a a a a a a a \tuplet 3/2 8 { bes[ d c] bes[ a g] } c8 d | g,4 r8 d'\p bes bes bes bes | a a a a g g g g |

  fis8 fis fis fis fis' fis fis fis | g g, g g d' d d d | d d cis cis d d d d |
  f16 f f f f f f f bes, bes bes bes bes bes bes bes | g' g g g g g g g c, c c c c c c c |
  a16 a  a a a a a a d8 d d d | es es es es c c c c |

  d8 d d d bes bes bes bes | c c c c a a a a |
  f'8 f f f f f f f | f\f[ es16 d c bes a g] f8 f f f | d'\pp d d d  c c c c |
  bes bes bes bes f' f f f | d' d c c bes bes a a |

  g8 g f f es\f es d d | es es f f bes, f'16 f d d bes bes | es8 es f f bes, d d bes' |
  bes es, es a a d, d g | f f f f f f f f | f'16 f d d bes bes d, d es8 es f f |
  bes,4\fermata \fine r8 b\p b b b d | d d b b g g g g | g g g g g g g g |

  g'16\f f es d c bes aes g aes8 f f f | f'16 es d c bes aes g f g8 es es es |
  es'16 d c bes aes g f es g8 g g g | g g g g g g g g | g g g g g g g g |
  c8 a' a a a a a a | g  g g g d'16 c bes a g f es d | es es f g c, es f g es' d c bes c bes a g |

  d8 d d d d d d d | d d d d d d d d | d d d d g,2\fermata \bar "|."\dc
}
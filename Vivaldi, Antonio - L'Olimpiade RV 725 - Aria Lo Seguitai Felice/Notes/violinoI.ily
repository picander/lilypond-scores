\version "2.19.82"

violinoI = \relative c'{
 \clef treble
 \key bes \major
 \time 4/4
 \partial 8
  \tempo "Allegro non molto"
  f8\p | bes16-. bes-. c-. bes-. bes-. bes-. c-. bes-. f'4\trill~f8~f32 es d c | bes16-. bes-. c-. bes-. bes-. bes-. c-. bes-. f'4\trill~f8~f32 es( d c64 bes) | a16-. bes-. c-. d-. es-. f-. g-. a-. \stemUp bes8 bes,, bes bes | \stemNeutral
  \tuplet 3/2 8 {bes''16\f[ f d] bes[ d f] bes[ bes, bes] bes[ bes bes] g[ es bes] g[ bes es] } g8 r | \tuplet 3/2 8 {c'16[ g e] c[ e g] c[ c, c] c[ c c] a[ f c] a[ c f]} a8 r |
  \tuplet 3/2 8 { bes'16[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes]} | es,8 es f f bes,4 r | \tuplet 3/2 8 { bes''16\p[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes]} | es,8 es f f bes,4 r8 f'\p | bes16-. bes-. c-. bes-. bes-. bes-. c-. bes-. f'4\trill~f8~f32 es d c |

 bes16-. bes-. c-. bes-. bes-. bes-. c-. bes-. f'4\trill~f8~f32 es( d c64 bes) | a16-. bes-. c-. d-. es-. f-. g-. a-. \stemUp bes8 bes,, bes bes | \stemNeutral
\tuplet 3/2 8 {bes''16[ f d] bes[ d f] bes[ bes, bes] bes[ bes bes] g[ es bes] g[ bes es] } g8 r | \tuplet 3/2 8 {c'16[ g e] c[ e g] c[ c, c] c[ c c] a[ f c] a[ c f]} a8 r |
\tuplet 3/2 8 {d'16[ a fis] d[ fis a] d[ d, d] d[ d d]} bes'2~| bes16 g es c es,8 r a'2~| a16 f d bes d,8 r8 g'2~|

g16 es c d c,8 r fis'4. a,8 | bes4( c) d( es) | d8 c bes a \tuplet 3/2 8 {g'16[ d c] bes[ a g] g'[ d c] bes[ a g]} |
\tuplet 3/2 8 { g'16[ d c] bes[ a g] g'16[ d c] bes[ a g] g'16[ d c] bes[ a g] g'16[ d c] bes[ a g]} | \tuplet 3/2 8 { g'16[\f d c] bes[ a g] g'16[ d c] bes[ a g] } c,8 c d d | \tuplet 3/2 8 { g16[ bes c] d[ e fis] } g8 g,, \tuplet 3/2 8 { es'!16[  f! g] a[ bes c] c[ d es] es[ f g] } |
\tuplet 3/2 8 {fis16 a g] fis[ e d] c[ d e] d,[ e fis] g[ d c] bes[ a g]} c8 d | g,4 r8 d'\p g16 g a g g g a g | d'8 d, r d' g16 g a g g g a g |

d'8 d, r d c16-. c-. d-. c-. c-. c-. d-. c-. | bes-. bes-. c-. bes-. bes-. bes-. c-. bes-. a-. a-. bes-. a-. a-. a-. bes-. a-. | g-. g-. a-. g-. g-. g-. a-. g-. fis8 d d d |
\tuplet 3/2 8 { bes''16[ f! d] bes[ d f] bes[ bes, bes] bes[ bes bes] g[ es bes] g[ bes es] } g8 r | \tuplet 3/2 8 { c'16[ g e] c[ e g] c[ c, c] c[ c c] a[ f c] a[ c f]} a8 r |
\tuplet 3/2 8 { d'16[ a fis] d[ fis a] d[ d, d] d[ d d]} bes'2~| \tuplet 3/2 8 { bes16[ bes a] g[ f es] } bes'4 a2~|

\tuplet 3/2 8 { a16[ a g] f[ es d] } a'4 g2~| \tuplet 3/2 8 { g16[ g f] es[ d c] } g'4 \tuplet 3/2 8 { f16[ a, bes] c[ bes a] } f'8 c |
d4( es) f( g) | f8\f[ es16 d c bes a g] \tuplet 3/2 8 { f16[ a bes] c[ bes a] } f'8 r | d16\pp d es d d d es d d d es d d d es d |
d d es d d d es d c c d c c c d c | \tuplet 3/2 8 { bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[ f es] d[ c bes]  } |

\tuplet 3/2 8 { bes'[ f es] d[ c bes] bes'[ f es] d[ c bes] bes'[\f  f es] d[ c bes] bes'[ f es] d[ c bes]  } | es,8 es f f bes, f'16 f d d bes bes | es8 es f f \tuplet 3/2 8 { bes,16[ d' es] f[ es d] bes'[ a g] f[ es d] } |
\tuplet 3/2 8 { es16[ c d] es[ d c] a'[ g f] es[ d c] d[ bes c] d[ c bes] g'[ f es] d[ c bes] | a[ f g] a[ g f] bes[ f g] a[ g f] c'[ f, g] a[ g f] d'[ f, g] a[ g f] } | f'16 f d d bes bes d, d es8 es f f |
bes,4\fermata \fine r8 d'\p d g, g aes | aes aes g g d' d d d | d d d d c c c c |

g'16\f f es d c bes aes g aes f g aes aes, f' g aes | f' es d c bes aes g f g es f g g, es' f g |
es'16 d c bes aes g f es f b c d g,, b' c d | g,, c' d es  g,, c' d es g,, d'' es f g,, d'' es f | g,, es'' f g g,, es'' f g g,,8 aes' g16 f es d |
c8 es' es es es es es es | es es d d d16 c bes a g f es d | es es f g c, es f g es' d c bes c bes a g |

d fis g a d, fis g a d, g a bes d, g a bes | d, a' bes c d, a' bes c d, bes' c d d, bes' c d | d,8 es' d16 c bes a g2\fermata \bar "|."\dc
 }
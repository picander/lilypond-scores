\version "2.19.82"

\include "../global.ily"
\include "../../commands.ily"

\include "../Notes/violinoI.ily"
\include "../Notes/violinoII.ily"
\include "../Notes/viola.ily"
\include "../Notes/canto.ily"
\include "../Notes/basso.ily"

#(set-global-staff-size 18)

\score {
  <<
    \new StaffGroup {
      <<
        \new Staff \with {
          instrumentName = "Violino I"
          shortInstrumentName = "Vl. I"
          midiInstrument = "violin"
        }\violinoI

        \new Staff \with {
          instrumentName = "Violino II"
          shortInstrumentName = "Vl. II"
          midiInstrument = "violin"
        }\violinoII

        \new Staff \with {
          instrumentName = "Viola"
          shortInstrumentName = "Vla."
          midiInstrument = "viola"
        }\viola
      >>
    }

    \new StaffGroup {
      <<
        \new Staff \with {
          vocalName = "Megacle"
          shortVocalName = "Meg."
        }
        <<
          \new Voice = "megacle"  {\megaclenotes}
        >>
          \new Lyrics \lyricsto "megacle" {\megaclelyrics}
      >>
    }

    \new StaffGroup {
      \override StaffGroup.SystemStartBracket.collapse-height = #4
      \override Score.SystemStartBar.collapse-height = #4
      \new Staff \with{
        instrumentName = "Basso"
        shortInstrumentName = "Basso"
        midiInstrument = "cello"
      }\basso
    }
  >>
  \layout {}
  \midi {}
}
\version "2.19.82"
\include "../../stylesheet.ily"

\header {
  title = \markup \scoretitle "Lo seguitai felice (Megacle)"
  subtitle = \markup \scoresubtitle "L'Olimpiade, RV 725"
  subsubtitle = \markup \scoresubsubtitle "Atto III, Sc. 3"
  composer = \markup \scorecomposer "Antonio Vivaldi"

  tagline = \markup {\override #'(font-name . "Perpetua") "Typesetting by Picander with Lilypond. License CC BY-NC 4.0."}
}
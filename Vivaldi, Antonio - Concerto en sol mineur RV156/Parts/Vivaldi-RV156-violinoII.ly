\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/violinoII.ily"

%Parts: Violino II

\book {
  \header {
    instrument = \markup \scoreinstrument "Violino II"
  }
  %partI
  \score {
    \new Staff \with {
      %instrumentName = "Violino II"
    }\violinoII-partI
    \header {
      piece = \markup "I. Allegro"
    }
  }

\pageBreak

  %partII
  \score {
    \new Staff \with {
      %instrumentName = "Violino II"
    }\violinoII-partII
    \header {
      piece = \markup "II. Adagio"
    }
  }

  %partIII
  \score {
    \new Staff \with {
      %instrumentName = "Violino II"
    }\violinoII-partIII
    \header {
      piece = \markup "III. Allegro"
    }
  }
}
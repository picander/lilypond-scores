\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/violinoI.ily"

%Parts: Violino I

\book {
  \header {
    instrument = \markup \scoreinstrument "Violino I"
  }
  %partI
  \score {
    \new Staff \with {
      %instrumentName = "Violino I"
    }\violinoI-partI
    \header {
      piece = \markup "I. Allegro"
    }
  }

\pageBreak

  %partII
  \score {
    \new Staff \with {
      %instrumentName = "Violino I"
    }\violinoI-partII
    \header {
      piece = \markup "II. Adagio"
    }
  }

  %partIII
  \score {
    \new Staff \with {
      %instrumentName = "Violino I"
    }\violinoI-partIII
    \header {
      piece = \markup "III. Allegro"
    }
  }
}
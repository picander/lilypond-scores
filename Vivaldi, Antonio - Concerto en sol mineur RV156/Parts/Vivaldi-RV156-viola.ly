\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/viola.ily"

%Parts: Viola

\book {
  \header {
    instrument = \markup \scoreinstrument "Viola"
  }
  %partI
  \score {
    \new Staff \with {
      %instrumentName = "Viola"
    }\viola-partI
    \header {
      piece = \markup "I. Allegro"
    }
  }

\pageBreak

  %partII
  \score {
    \new Staff \with {
      %instrumentName = "Viola"
    }\viola-partII
    \header {
      piece = \markup "II. Adagio"
    }
  }

  %partIII
  \score {
    \new Staff \with {
      %instrumentName = "Viola"
    }\viola-partIII
    \header {
      piece = \markup "III. Allegro"
    }
  }
}
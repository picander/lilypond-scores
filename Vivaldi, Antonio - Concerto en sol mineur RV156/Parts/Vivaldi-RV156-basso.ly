\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/basso.ily"

%Parts: Viola

\book {
  \header {
    instrument = \markup \scoreinstrument "Basso"
  }
  %partI
  \score {
    \new Staff \with {
      %instrumentName = "Basso"
    }\basso-partI
    \header {
      piece = \markup "I. Allegro"
    }
  }

\pageBreak

  %partII
  \score {
    \new Staff \with {
      %instrumentName = "Basso"
    }\basso-partII
    \header {
      piece = \markup "II. Adagio"
    }
  }

  %partIII
  \score {
    \new Staff \with {
      %instrumentName = "Basso"
    }\basso-partIII
    \header {
      piece = \markup "III. Allegro"
    }
  }
}
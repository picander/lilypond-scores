\version "2.19.82"

viola-partI = \relative c'' {
  \clef alto \key d \minor \time 4/4
  bes4 bes a a | a a g g | g g a a |
  a a bes bes | a a a a | g g g g |

  a a a a | g g g es | c f f d |
  bes es es c | a a' a fis | \editaltb fis?\editalte fis d d |

  bes4 bes' a d, | g a bes d, | d d e c |
  a d d d | e f e f | e f a8 a a a |

  a4 f e e | e e d d | d d e e |
  e e d f | e e e e | d d d d |

  e4 e e e | d d d g | g c, c f |
  f bes, bes e | e a, f f' | e a d, e |

  f a a a | d, f e e | cis cis cis cis |
  cis cis d a' | a a, c c | c8 c d c c c d c |

  c4 a' g g | g g f f | f f g g |
  g g f a | g g g g | f f f f |

  g g g g | f f f d | bes e e c |
  a d d g | g c, c d | c d c d |

  c a f f' | f d c c | c c bes bes |
  bes bes c c | c c bes d | c c c c |

  bes bes bes bes | c c c c | bes r g' g |
  g r g g | aes d, g g | g es d d |

  d d c c | c c d d | d d c es |
  d d d d | c c c c | d d d d | c c a' a |

  g bes, bes bes | bes bes bes bes | bes bes' a a |
  a a g g | g g a a | a a d, bes' |

  a d, g a | bes d, d d | e c a d | d8 d d d bes2\fermata \bar "|."
}

viola-partII = \relative c'' {
  \clef alto \key d \minor \time 4/4
  bes2~bes4 g8 es | a,4 d e d~| d8 c d b e4. a8 | f4 d e c |

  d4 bes c8 bes c a | d[ b'] g[ c] aes4 \editaltb bes\editalte | g aes f g | es f8 d bes4 bes' |
  c2 bes~| bes4 g8 es a,4 d8 g | e4. cis'8 a fis d bes | es c d4 d2 | es8 c d4 d2\fermata \bar "|."
}

viola-partIII = \relative c'' {
  \key d \minor \clef alto \time 3/8
  R4. |R4. |R4. | R4. | R4. |
  R4. | d,16 d d d d d | g g f es d c | bes bes bes bes bes bes | es es d c bes a |

  g8 g'4~ | g16 bes a g f es | c' es d c bes a | f a g f es d | bes' d c bes a g |
  es g f es d c | a' c bes a g fis | d8 d r16 d | d8 d r16 d | d8 d r16 d | fis fis fis fis fis fis |

  fis fis fis fis fis fis | d d d d d d | e e e e e e | a,4 a'8 | e a4 | f4 r8 |
  f4 r8 | f4 r8 | f4 r8 | f4 r8 | f4 r8 |

  a,16 a a a a a | d d c bes a g | f f f f f f | bes bes a g f e | d8 d'4~|
  d16 f e d c bes | g' bes a g f e | c e d c bes a | f' a g f e s | bes d c bes a g | e' g f e d cis |

  a8 a r16 a' | a8 cis, r16 a' | a8 f r16 a | cis, cis cis cis cis cis | cis cis cis cis cis cis | a[ a a a] a'[ a] |
  b16 b b b b b | g4 g8 | f d g | es4 r8 | \editaltb es!4 r8 | es!4 r8  |

  es!4 r8 | es!4 r8 | es!4 r8 \editalte | g16 g g g g g | c c bes aes g f |
  es16 es es es es es | aes aes g f es d | c8 c'4~| \editaltb c16 es!\editalte d c bes  aes | f aes g f es d |

  bes' d c bes aes g | es g f es d c | aes' c bes aes g f | d f es d c b | g8 g' r16 g |
  g8 b, r16 g' | g8 c, r16 g' | g g g g g g | g g g g g g | g g g g g g | a a a a a a |

  f16 f f f f f | f8 g f | d4 r8 | d4 r8 | d4 r8 | d4 r8 |
  bes4 r8 | g'16 bes a g f es | c' \editaltb es!\editalte d c bes a | f a g f es d | bes' d c bes a g | es g f es d c |
  a'16 c bes a g fis | d bes bes bes bes bes | bes bes bes bes bes bes | bes bes bes bes bes bes | bes bes bes bes bes bes | g d' d d d d |
  d d d d d d | d d d d d d | d d d d d d | d d d d d d | d d d d d d | d4.\fermata \bar "|."
}
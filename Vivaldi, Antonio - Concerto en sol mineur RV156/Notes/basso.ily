\version "2.19.82"

basso-partI = \relative c' {
  \key d \minor \clef bass \time 4/4
  g4 g fis fis | f! f e e | es es d d |
  fis fis g g | fis fis f f | e! e es es |

  d4 d fis fis | g g c, c | f f bes, bes |
  es4 es a, a | d d d d | d d d d |

  g, g' d' b | c fis, g g, | d' b' c c, |
  d4 c bes bes' | a a, a' a, | a' a, a'8 d, a' a, |

  d4 d cis cis |  c! c b b | bes! bes a a |
  cis cis d d | cis cis c c | b b bes bes |

  a4 a a a | d d g g | c, c f f |
  bes, bes e e | a, a d d | a' fis g cis, |

  d4 d' a fis | g g a a, | a' a, a' a, |
  a' a, d d, | d' d, e' c |f,8 f' bes, c f, f' bes, c |

  f,4 f' e e | e e d d | d d c c |
  e e f f | e e e e | d d d d |

  c4 c e e | f f bes, bes | e e a, a |
  d d g g | es es f f, | f' f, f' f, |

  f' f,  bes f' | bes, bes' a a | a a g g |
  g g f f | a, a bes bes' | a a a a |

  g4 g g g | f f a, a | bes d b g |
  c c' c, c' | f, b, c g' | c, c b b |

  bes! bes a a | aes aes g g | b b c c |
  b b bes bes | a a aes aes | g g b b | c c fis fis |

  g g, g' g, | g' g, g' g, | g' g fis fis |
  f! f e e | es es d d | fis fis g g |

  d' b c fis, | g g, d' b' | c c, d d, | g8 g d' d, g2\fermata \bar "|."
}

basso-partII = \relative c' {
  \key d \minor \clef bass \time 4/4
  g8 fis g g,  es' d es c | d a' bes g cis, a' d d, | gis, a b gis a b cis a | bes a bes g a g a f |

  g8 f g c f g a f | g[ g,] c[ es] f[ d] g[ g,] | \editaltb e'!\editalte c f f, d' bes es es, | c' aes' d, bes es g f es |
  fis, g a fis g g' bes g | es d es c d a' bes g | a b cis a d d, g g, | c a d d,  g' fis g g, | c a d d, g2\fermata \bar "|."

}

basso-partIII = \relative c' {
 \key d \minor \clef bass \time 3/8
 R4. | R4. | R4. | R4. | g,16 g g g g g |
 g'16 g f es d c | bes bes bes bes bes bes | es es d c bes a | g[ a32 bes] c[ d e fis] g16 r | g,[ a32 bes] c[ d e fis] g16 r |

 g16 g g g g g | c, c c c c c | f f f f f f | bes, bes bes bes bes bes | es es es es es es |
 a, a a a a a | d d d d d d | g g g g g g | d d d d d d | g g g g g g | d d d d d d |

 d d d d d d | g g g g g g | cis, cis cis cis cis cis | d4 d8 | g a a, | d4 r8 |
 d4 r8 | d4 r8 | d4 r8 | d16 d d d d d | d' d c bes a g |

 f16 f f f f f | bes bes a g f e | d[ e32 f] g[ a b cis] d16 r |d,[ e32 f] g[ a b cis] d16 r | d, d d d d d |
 g g g g g g | c, c c c c c | f f f f f f | bes, bes bes bes bes bes | e e e e e e | a, a a a a a |
 
 d16 d d d d d | a a a a a a | d d d d d d | a a a a a a | a a a a a a | d d d d d d |
 g,16 g g g g g | c4 c'8 | f, g g, | c4 r8 | c4 r8 | c4 r8 |
 
 c4 r8 | c16 c c c c c | c' c bes aes g f | es es es es es es | aes aes g f es d |
 c16[ d32 es] f[ g a b] c16 r | c,[ d32 es] f[ g a b] c16 r | c,16 c c c c c | f f f f f f | bes, bes bes bes bes bes |
 
 es16 es es es es es | aes, aes aes aes aes aes | d d d d d d | g, g g g g g | c c c c c c |
 g16 g g g g g | c c c c c c | b b b b b b | g g g g g g | c c c c c c | f, f f f f f |

 bes4 r8 | bes8 es f | bes4 r8 | bes4 r8 | bes4 r8 | bes4 r8 |
 bes,16 bes bes bes bes bes | c c c c c c | f f f f f f | bes, bes bes bes bes bes | es es es es es es | a, a a a a a |
 
 d16 d d d d d | g, g g g g g | g g g g g g | g g g g g g | g g g g g g | g g g g g g |
 d' d d d d d | d, d d d d d | g g g g g g | d' d d d d d | d, d d d d d | g4.\fermata \bar "|."
 
}
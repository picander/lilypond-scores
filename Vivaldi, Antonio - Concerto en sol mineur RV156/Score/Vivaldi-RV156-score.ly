\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "../Notes/violinoI.ily"
\include "../Notes/violinoII.ily"
\include "../Notes/viola.ily"
\include "../Notes/basso.ily"
\include "../Notes/continuo.ily"

%Score
#(set-global-staff-size 17)

\book {
  %part I
  \score {
    <<
      \new StaffGroup {
        <<
          \new Staff \with {
            instrumentName = "Violino I"
            midiInstrument = "violin"
            shortInstrumentName = "Vl. I"
          }\violinoI-partI

          \new Staff \with {
            instrumentName = "Violino II"
            midiInstrument = "violin"
            shortInstrumentName = "Vl. II"
          }\violinoII-partI
          \new Staff \with  {
            instrumentName = "Viola"
            midiInstrument = "viola"
            shortInstrumentName = "Vla."
          }\viola-partI
          \new Staff \with {
            instrumentName = "Basso"
            midiInstrument = "cello"
            shortInstrumentName = "Bas."
          }\basso-partI
        >>
      }
      \new Staff \with {
        instrumentName = "Continuo"
        midiInstrument = "cello"
        shortInstrumentName = "Cont."
      }<< \continuo-partI \new FiguredBass \chiffres-partI >>
    >>
    \header {
      piece = "I. Allegro"
    }
    \layout {}
    \midi {}
  }
\pageBreak
  %part II
  \score {
    <<
      \new StaffGroup {
        <<
          \new Staff \with {
            instrumentName = "Violino I"
            midiInstrument = "violin"
            shortInstrumentName = "Vl. I"
          }\violinoI-partII

          \new Staff \with {
            instrumentName = "Violino II"
            midiInstrument = "violin"
            shortInstrumentName = "Vl. II"
          }\violinoII-partII
          \new Staff \with  {
            instrumentName = "Viola"
            midiInstrument = "viola"
            shortInstrumentName = "Vla."
          }\viola-partII
          \new Staff \with {
            instrumentName = "Basso"
            midiInstrument = "cello"
            shortInstrumentName = "Bas."
          }\basso-partII
        >>
      }
      \new Staff \with {
        instrumentName = "Continuo"
        midiInstrument = "cello"
        shortInstrumentName = "Cont."
      }<< \continuo-partII \new FiguredBass \chiffres-partII >>
    >>
    \header {
      piece = "II. Adagio"
    }
    \layout {}
    \midi {
    }
  }
\pageBreak
  %part III
  \score {
    <<
      \new StaffGroup {
        <<
          \new Staff \with {
            instrumentName = "Violino I"
            midiInstrument = "violin"
            shortInstrumentName = "Vl. I"
          }\violinoI-partIII

          \new Staff \with {
            instrumentName = "Violino II"
            midiInstrument = "violin"
            shortInstrumentName = "Vl. II"
          }\violinoII-partIII
          \new Staff \with  {
            instrumentName = "Viola"
            midiInstrument = "viola"
            shortInstrumentName = "Vla."
          }\viola-partIII
          \new Staff \with {
            instrumentName = "Basso"
            midiInstrument = "cello"
            shortInstrumentName = "Bas."
          }\basso-partIII
        >>
      }
      \new Staff \with {
        instrumentName = "Continuo"
        midiInstrument = "cello"
        shortInstrumentName = "Cont."
      }<< \continuo-partIII \new FiguredBass \chiffres-partIII >>
    >>
    \header {
      piece = "III. Allegro"
    }
    \layout {}
    \midi {}
  }
}
\version "2.19.82"

basso = \relative c{
  \clef bass
  \key f \major
  \time 2/4
  
  \tempo "Allegro"
  \partial 8 r8 | r8 f16 e f8 f, | r8 f'16 e f8 f, | r e'16 d e8 e, | r8 f'16 e f8 f, |  
  r8 f'16 e f8 f, | c'4\p c | c c | c c | c c |  
  f,\f r | f8 f' c' c, | f,16 f' c a f4 | f2~| f4\fermata r8
  
  \repeat volta 2 {
   r8 | r f'16 e f8 f, | r f'16 e f8 f, | r8 e'16 d e8 e, | f' f, f r\fermata |   
   r8 c'16 d e8 c | r a'16 g f8 fis | g4 g, | g g |   
   g g | g g | g8[ g a b?] | c c, g'' g, | c16 c' g e c4 |
  }
  
  \repeat volta 2 {
   c4 r | c r | c r | c r |    
   c8[ d e cis] | a a'16 g a8 a, | d r g r | c, r f r | b, r e r |   
   a,4 a | a a | a'8[ a, b cis] | d[ d, g a] | d16 d' a f d4 |
  }
  
  \repeat volta 2 {
    r8 a'[ a a,] | r e'[ gis, e] | a[ a' a a,] | a4 a |     
    d d | g f | e e | a a, |    
    d f | e8[ e e e] | a,[ a a a] | gis[ gis gis gis] | 
    a4 r | a8[ a gis gis] | a[ a e' e,] | a16 a' e c a4 |
  }
    
    r8 f'16 e f8 f, |    
    r8 f'16 e f8 f, | r f'16 e f8 f, | r f'16 e f8 f, | r c''16 bes c8 c, | r f16 e f8 f, |    
    r d'16 c d8 d, | r g'[ c, d] | g, g'16 fis g8 g, | r8 e'16 d e8 e, |    
    r8 a'[ d, e] | a, a'16 b c8 c, | f, f'16 e f8 a, | bes bes'16 a bes8 d, | c c'16 bes c8 c, |    
    f[ g a bes] | c4 r | c,8[ d c c,] | f16 f' c a f4 | r8 f'16 e f8 f, |    
    c'4 e | e e | f,8 f'16 e f8 f, | f4 r8 c' | f,f'16 e f8 c |
    
    
    f,4 r8 c' | f, f'16 e f8 c | f, f'16 e f8 f, | r bes'16 a bes8 bes, | r8 c'16 bes c8 c, |    
    f8[ g a bes] | c4 r | c,8[ d c c,] | f\f f'16 e f8 f, | r f'16 e f8 f, |
    r8 c'16\p bes c8 c, | r c'16 bes c8 c, | f4\f r | f8 f' c c, | f2\fermata \bar "|."
}
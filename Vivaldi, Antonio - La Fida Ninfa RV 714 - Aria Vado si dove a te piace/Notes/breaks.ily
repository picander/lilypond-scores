\version "2.19.82"

breaks = {
    \partial 8 s8 |
    s2 * 13
    s4. 				
    \break
    s8 
    %m. 15
    s2 * 13
    \break
    %m. 28
    s2 * 14
    \break
    %m. 42
    s2 * 16
    \break
    %m. 58
    s2 * 40
}
\version "2.19.82"

cantonotes = \relative c''{
  \autoBeamOff
  \clef treble
  \key f \major
  \time 2/4
  
  \tempo "Allegro"
  \partial 8 r8 | R2 | R2 | R2 | R2 |  
  R2 | R2 | R2 | R2 | R2 |  
  R2 | R2 | R2 | \tempo Adagio c4. a8 | f4\fermata r8 
  \repeat volta 2 {
    a16 bes | c8 f4 e16 d | c[ bes] c4 bes16 a | bes8 d4 c16 bes a[ g] f8 c' r\fermata    
    c16[ d e f] g8 g, | a f' d c | b16[ c b g] c[ d c g] | d'[ e d g,] e'[ f e g,] |
    f'[ d8.] e16[ c8.] | d16[ b8.] c16[ a8.] | g8 a' g f | e[ d16 c] d4\trill | c r |
  } 
  \repeat volta 2 {
    c,8[ d16 e] f[ g a b] | c[ g a b] c[ d e f] | g[ e f d] e[ c d b] | c[ g a b] c[ d] e[ f] |     
    g8 r cis, r | r e a, e' | f a, b f' | e gis, a e' | d fis, gis? d' |    
    cis16[ a a8]\trill d16[ a a8]\trill | e'16[ a, a8]\trill f'16 a, a8\trill | g'[ f16 e] d16[ cis] b[ a] | f'8 e16[ d] e8[ d16 cis] | d4 r |
  }
  \repeat volta 2 {
    a16[ b c d] e8 a, | gis f' e d | d4\trill( c) | e8 a, r e' |    
    \appoggiatura e8( f4) r8 a, | bes! g r b | \appoggiatura b8( c4) r8 e | cis a r e' |    
    \appoggiatura e8( f4) r8 a, | b16[ a8 gis16] d'16[ c8 b16] | c16[ b8 a16] e'16[ d8 c16] | d16[ c8 b16] f'16[ e8 d16] |    
    c16[ a b c] d[ e fis gis] | a8 a, r e' | c[ b16 a] b4\trill | a r |
  }
   c4. bes16[ a] |
   
   \appoggiatura g8( f8) f r c' | d f4 e16[ d] | c[ bes]  c4 bes16[ a] | bes8 g'4 bes,8 | a16[ g] f8 c' es |   
   es fis, fis d' | \appoggiatura c8( bes) a16[ g] \appoggiatura bes8( a8) g16[ fis] | \appoggiatura fis8( g4) d'8 f | f gis, gis e' |   
   
   \appoggiatura d8 c b16[ a] \appoggiatura c8 b8 a16[ gis] | \appoggiatura g8 a4 r | c c, | d bes' | e, c' |   
   a16[ bes] c8 f e16[ d] | c8[ bes16 a] g[ f] e d | c8 f g4\trill | f r | aes8 c4 aes8 |   
   g8 bes4 des8~| des8 c4 bes8 | aes16[ g] f8 r4 | f8[ g16 a] bes[ c d e] | f8 f, r4 |
   
   f8[ g16 a] bes[ c d e] | f8 f, c' r | r4 f,8 c' | d c16[ bes] bes8 f' | e d16[ c] c8 e |
   f8 r f e16[ d] | c8[ bes16 a] g[ f] e d | c8 f g4\trill | f r | R2 |
   R2 | R2 | R2 | R2 | R2^\fermataMarkup \bar "|."
}

cantotext = \lyricmode {
 Va -- do, sì, 
 va -- do, sì, do -- ve~a te pia -- ce, ma non spe -- ro~a -- ver mai pa -- ce, ma,
 ma __ _ non spe -- ro~a -- ver mai | pa -- _ _ _ _ _ _ _ _ ce,~a -- ver mai | pa -- _ ce, 
 
 cor -- _ _ _ _ _ _ _ ro,
 sì, ma, ma~in o -- gni lo -- co di for -- tu -- na sa -- rò gio -- co, sa -- rò
 gio -- _ _ _ _ _ _ _ co, sa -- rò gio -- co,
 
 poi -- _ ché | me -- co~o -- gnor ver -- rà __ i -- ra, a -- 
 mor, spa -- ven -- to e duol, spa -- ven -- to, a --
 mor, spa -- ven -- - - - - - 
 
 - - - to, spa -- ven -- to~e duol
 O -- v'io va -- da o pa -- dre a -- ma -- to, o pa -- dre a -- ma -- to, il mio 
 fa -- to, ri -- tro -- var ben mi sa -- prà, il mio fa -- to, ri -- tro --
 
 var ben mi sa -- prà, ben -- ché~a-- sco -- sa~a~i rai del 
 so -- le, ben -- ché~a -- sco -- - sa a~i rai del so -- le, pa -- dre a --
 ma -- - - - - - to, va -- - - do,
 
 cor -- - - ro, ma, ma~il mio fa -- to, ri -- tro -- var ben mi sa --
 prà, ben -- ché~a -- sco -- - sa a~i rai del so -- le.
 
}
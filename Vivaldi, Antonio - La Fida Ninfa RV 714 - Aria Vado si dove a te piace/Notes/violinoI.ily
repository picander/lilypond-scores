\version "2.19.82"
\include "../../commands.ily"

violinoI = \relative c''{
  \clef treble
  \key f \major
  \time 2/4
  \tempo "Allegro"
  \partial 8 a16 bes | c8 f4 e16 d | c16 bes c4 bes16 a | bes8 d4 c16 bes | a g f8\trill c'16 a a8\trill |
  f'16 c c8\trill a'16 f f8\trill | g16(\p e8.) bes'16( g8.) | a16( f8.) g16( e8.) | f16( c8.) bes'16( g8.) | a16( f8.) g16( e8.) |
  f16\f a g f e d c bes | a8 f' g4\trill | f r | R2 | \grpar r4 -\parenthesize \fermata r8
 
  \repeat volta 2 {
    a,16\p bes | c8 f4 e16 d | c16 bes c4 bes16 a | bes8 d4 c16 bes | a g f8 c'8 r \fermata 
    c16 d e f g8 g, | a f' d c | b16 c b g c d c g | d' e d g, e' f e g, |
    
    f'( d8.) e16 c8. | d16 b8. c16 a8. | g8[ a' g f] | e d16 c d4\trill | c r
  }
  
  \repeat volta 2 {
   c,8 d16 e f g a b | c g a b c d e f | g e f d e c d b | c g a b c d e f |
   g8 r cis, r | r e a, e' | f[ a, b f'] | e[ gis, a e'] | d[ fis, gis? d'] |
   
   cis16 a a8\trill d16 a a8\trill | e'16 a, a8\trill f'16 a, a8\trill | g'8 f16 e d cis b a | f'8 e16 d e8 d16 cis | d4 r 
  }
  
  \repeat volta 2 {
   a16 b c d e8 a, | gis[ f' e d] | d4\trill( c) | e16 c a c e c a e' |   
   f d a d f d a d | d bes g d' d b g b | c g e c' c g e e' | e cis a e' e cis a e' |   
   f d a f' f d a f' | b, a8 gis16 d'16 c8 b16 | c b8 a16 e' d8 c16 | d c8 b16 f' e8 d16 |
   c16 a b c d e fis gis | a8 a, r e' | c b16 a b4\trill | a r | 
  }

   c4. bes16 a |   
   \appoggiatura g8 f8 f r c' | d f4 e16 d | c bes c4 bes16 a | bes8 g'4 bes,8 | a16 g f8 c' es |   
   es[ fis, fis d'] | \appoggiatura c8 bes8 a16 g \appoggiatura bes8 a8 g16 fis | \appoggiatura fis8 g4 d'8 f | f[ gis, gis e'] |   
   
   \appoggiatura d8 c8 b16 a \appoggiatura c8 b8 a16 gis | \appoggiatura gis8 a4 r | c c, | d bes' e, c' |   
   a16 bes c8  f e16 d | c8 bes16 a g f e d | c8 f g4\trill | f r aes8 c4 aes8 |   
   g bes4 des8~| des8 c4 bes8 | aes16 g f8 r4 | f8 g16 a bes c d e | f8 f, r4 |   
   
   f8 g16 a bes c d e | f8 f, c' r | r4 f,8 c' | d c16 bes  bes8 f' | e d16 c c8 e |   
   f r f e16 d | c8 bes16 a g f e d | c8 f g4\trill | a16\f f f8\trill c'16 a a8\trill | f'16 c c8\trill a'16 f f8\trill |    
   g16(\p e8.) bes'16( g8.) | a16( f8.) g16( e8.) | f16 a g f e d c bes | a8 f' g4\trill | f2\fermata \bar "|."   
  }
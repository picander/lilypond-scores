\version "2.19.82"
\include "../../commands.ily"

violinoII = \relative c'{
  \clef treble
  \key f \major
  \time 2/4

  \tempo "Allegro"
  \partial 8 f16 g | a8 d4 c16 bes | a g a4 g16 f | g8 bes4 a16 g | f c c8 a'16 f f8\trill |
  c'16 a a8\trill f'16 c c8\trill | e16(\p c8.) g'16( e8.) | f16( c8.)  e16( c8.) | c16( a8.) g'16( e8.) | f16( c8.)  e16( c8.) |
  a'16 f e d c bes a g | f8 f' e4\trill | f4 r | R2 | \grpar r4 -\parenthesize \fermata r8

  \repeat volta 2 {
    f,16\p g | a8 d4 c16 bes | a g a4 g16 f | g8 bes4 a16 g | f e f8 a r\fermata |
    e8 g4 e8 | f a a4 | d, r | b'16 c b g c d c g |
    d'16( b8.) c16( g8.) | b16( g8.) e'16( c8.) | b8[ f' e d] | c4 b | c r
  }
  
  \repeat volta 2 {
    c,8 d16 e f g a b | c g a b c d e f | e c d b c g b g | e8 f16 g a b c d |
    e8 r g, r | r cis cis4\trill | d8 f,4 b8~| b e,4 a8~| a d,4 gis8 |
    e4 r | cis'16 a a8\trill d16 a a8\trill | e'4 r8 cis | a f g e | f4 r
  }

  
  \repeat volta 2 {
    c'4. c8 | b[ d b gis] | e2 | c'16 a e a c a e c' |
    d a f a d a f a | bes g d g b g d g | g e c g' g e c g' | cis a e cis' cis a e cis' |
    d a f d' d a f d' | gis, f8 e16 b' a8 gis16 | e' d8 c16 c b8 a16 | b a8 gis16 d' c8 b16 | a16 a b c d e fis gis | a8 a, r e' | c8 b16 a b4\trill | a4 r |
  }

  
  a4. g16 f |
  c'8 c r a | bes d4 c16 bes | a g a4 g16 f | g8 e'4 g,8 | f16 e f8 a c |
  c4. a8 | g[ bes c a] | bes4 bes8 d | d4. b8 |


  a8[ c d b] | a4 r | c c, | d bes' | e, c' |
  a16 bes c8 f e16 d | c8 bes16 a g f e d | c8 f g4\trill | f r | f8 aes4 f8 |
  e8 g4 bes8~| bes8 g4 e8 | c4 r | f8 g16 a bes c d e | f8 f, r4 |
  f8 g16 a bes c d e | f8 f, a r | r4 a8 a | bes8 d4 d8 | c e4 c8 |  
  a8 r f'8 e16 d | c8 bes16 a g f e d | c8 f g4\trill | f16\f c c8 a'16 f f8\trill | c'16 a a8\trill f'16 c c8\trill |  
  e16(\p c8.) g'16( e8.) | f16( c8.) e16( c8.) | a'16\f f e d c bes a g | f8 f' e4\trill | f2\fermata \bar "|."
}

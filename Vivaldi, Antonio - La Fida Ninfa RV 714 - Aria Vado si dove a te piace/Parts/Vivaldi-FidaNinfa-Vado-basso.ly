\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/breaks.ily"
\include "../Notes/basso.ily"

%Parts: Violino II

\book {
  \header {
    instrument = "Basso"
  }
  \score {
      \new Staff \with {
        %instrumentName = "Basso"
      }
      <<
      \new Voice {\breaks}
      \new Voice {\basso}
      >>
  }
}
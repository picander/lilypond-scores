\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/violinoI.ily"
\include "../Notes/breaks.ily"

%Parts: Violino I

\book {
    \header {
    instrument = "Violino I"
  }
\score {

  \new Staff \with {
    %instrumentName = "Violino I"
  }
  <<
  \new Voice {\violinoI}
  \new Voice {\breaks}
  >>
}
}
\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/viola.ily"
\include "../Notes/breaks.ily"

%Parts: Violino II

\book {
    \header {
    instrument = "Viola"
  }
\score {

  \new Staff \with {
    %instrumentName = "Viola"
  }
  <<
    \new Voice {\viola}
    \new Voice {\breaks}
  >>
}
}
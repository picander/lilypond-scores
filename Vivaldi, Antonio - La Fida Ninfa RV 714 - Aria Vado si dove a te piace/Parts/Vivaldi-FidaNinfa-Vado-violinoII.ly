\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "stylesheet-parts.ily"
\include "../Notes/violinoII.ily"
\include "../Notes/breaks.ily"

%Parts: Violino II

\book {
    \header {
    instrument = "Violino II"
  }
\score {

  \new Staff \with {
    %instrumentName = "Violino II"
  }
  <<
  \new Voice{\violinoII}
  \new Voice{\breaks}
  >>
}
}
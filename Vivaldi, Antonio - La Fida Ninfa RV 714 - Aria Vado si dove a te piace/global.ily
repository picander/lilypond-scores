\version "2.19.82"

\header {

  title =  \markup \scoretitle "Vado, si, dove a te piace"
  subtitle = \markup \scoresubtitle "La Fida Ninfa"
  subsubtitle = \markup \scoresubsubtitle "Atto 3, scena 5 (Licori)"
  composer = \markup \scorecomposer "Antonio Vivaldi"

  tagline = \markup {\override #'(font-name . "Perpetua") "Typesetting by Picander with Lilypond. License CC BY-NC 4.0." }
}
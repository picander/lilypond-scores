\version "2.19.82"

\include "../../commands.ily"
\include "../../stylesheet.ily"

\include "../global.ily"

\include "../Notes/violinoI.ily"
\include "../Notes/violinoII.ily"
\include "../Notes/viola.ily"
\include "../Notes/basso.ily"
\include "../Notes/canto.ily"

#(set-global-staff-size 17)

\book {
  \score {
    <<
      \new StaffGroup {
        <<
          \new Staff \with{
            instrumentName = "Violino I"
            shortInstrumentName = "Vl. I"
            midiInstrument = "violin"
          }\violinoI
          \new Staff \with{
            instrumentName = "Violino II"
            shortInstrumentName = "Vl. II"
            midiInstrument = "violin"
          }\violinoII
          \new Staff \with{
            instrumentName = "Viola"
            shortInstrumentName = "Vla."
            midiInstrument = "viola"
          }\viola
        >>
      }
      \new StaffGroup {
        <<
      \new Staff \with{
        vocalName = "Licori"
        shortVocalName = "Lic."
        midiInstrument = "choir aahs"
      }<< \new Voice = "canto" {\cantonotes} >>
        \new Lyrics \lyricsto "canto" {\cantotext}
        >>
      }
      
      \new StaffGroup {
        \override StaffGroup.SystemStartBracket.collapse-height = #4
        \override Score.SystemStartBar.collapse-height = #4
        \new Staff \with{
          instrumentName = "Basso"
          shortInstrumentName = "Basso"
          midiInstrument = "cello"
        }<<\basso>>
      }
    >>
    \layout{
    }
    \midi{
    }
  }
}

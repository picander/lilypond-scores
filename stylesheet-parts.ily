\version "2.19.82"

#(set-global-staff-size 19)

editaltb = \set suggestAccidentals = ##f
senzacemb = _\markup \italic "Senza Cem."

header-partI = \markup {
  \override #'(font-name . "Perpetua")
  \fontsize #1
  "Allegro"}

header-partII = \markup {
    \override #'(font-name . "Perpetua")
    \fontsize #1
    "Andante"}

header-partIII = \markup {
    \override #'(font-name . "Perpetua")
    \fontsize #1
    "Allegro"}

\layout {
      indent = 0\cm
    }
\paper {
    markup-system-spacing.basic-distance = #35

    score-system-spacing =
    #'((padding . 1)
       (basic-distance . 12)
       (minimum-distance . 6)
       (stretchability . 12))
    }
